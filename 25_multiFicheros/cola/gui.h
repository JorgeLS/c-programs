#ifndef __GUI_H__
#define __GUI_H__

#include <stdio.h>
#include <stdlib.h>

#include "general.h"

#define RED   "\x1B[31m"
#define CYAN  "\x1B[36m"
#define RESET "\x1B[0m"

#ifdef __cpluscplus
extern "C" {
#endif
void imprimir (struct TCola c);
void titulo ();
#ifdef __cpluscplus
}
#endif

#endif
