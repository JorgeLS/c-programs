#ifndef __GUI_H__
#define __GUI_H__

#include <stdio.h>
#include <stdlib.h>

#include "general.h"

#ifdef __cpluscplus
extern "C" {
#endif
    void titulo ();
    void imprimir (struct TPila p);
#ifdef __cpluscplus
}
#endif

#endif
