#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <strings.h>

#include "general.h"
#include "gui.h"

void push (struct TPila *p, int nuevo) {
    p->dato[p->cima] = nuevo;
    p->cima++;
}

int pop (struct TPila *p) {
    int dato;
    p->cima--;
    dato = p->dato[p->cima];

    return dato;
}

int  main(int argc, char *argv[]){
    struct TPila pila;
    int devuelto = 0;
    int nuevo;
    int d;
    bool fin = false;
    bzero (&pila, sizeof (pila));

    do {
        titulo ();
        imprimir(pila);
        printf ("\tDevuelto: %i\n", devuelto);
        printf ("\tEntrada: ");
        d = scanf (" %i", &nuevo);
        __fpurge(stdin);
        if (d)
            push (&pila, nuevo);
        else
            devuelto = pop(&pila);
    } while (!fin);


    return EXIT_SUCCESS;
}
