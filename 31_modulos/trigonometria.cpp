#include <math.h>
#include "trigonometria.h"

#define PI 3.14159265

const char *names[] = {
    "cosR",
    "sinR",
    "cosG",
    "sinG",
    "hypotenuse",
    (char *) 0
};

const char **catalogo () { return names; }
double cosR (int ang) { return cos(ang); }
double sinR (int ang) { return sin(ang); }
double cosG (int ang) { return cos(ang * 180 / PI); }
double sinG (int ang) { return sin(ang * 180 / PI); }
double hypotenuse (int c1, int c2) { return sqrt(pow(c1, 2) + pow(c2, 2)); }
