#include "multidivi.h"

const char *names[] = {
    "multi",
    "divi",
    // Valor centinela
    (char *) 0
};

const char** catalogo () { return names; }
double multi (double op1, double op2) { return op1 * op2; }
double divi (double op1, double op2)  { return op1 / op2; }
