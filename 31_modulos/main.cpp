#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

#define moduleName "librerias.cfgs"

typedef struct {
    char **nombreFun;
    int pos = 0;
}TNomFun;

TNomFun nomFun;

void getNombreFunciones (void* handle) {

    const char ** (*catalogo) ();

    catalogo = (const char ** (*) ()) dlsym (handle, "catalogo");

    const char **devuelto = (*catalogo)();

    for (const char **nombre = devuelto; *nombre != (char *) 0; nombre++) {
        nomFun.nombreFun = (char **) realloc (nomFun.nombreFun, (nomFun.pos + 1) * sizeof(char **));
        nomFun.nombreFun[nomFun.pos++] = strdup (*nombre);
    }
}

int main (int argc, char *argv[]) {

    FILE *librerias;
    void* handle;

    char nombreLib[0x100];

    if (argc <= 1) {
        if ( !(librerias = fopen(moduleName, "r") ) ) {
            fprintf (stderr, "No se abre el archivo: %s\n", moduleName);
            return EXIT_FAILURE;
        }
    }
    else {
        if ( !(librerias = fopen(argv[1], "r") ) ) {
          fprintf (stderr, "No se abre el archivo: %s\n", argv[1]);
          return EXIT_FAILURE;
        }
    }



    while (!feof(librerias)) {

        if ( fscanf (librerias, " %s", nombreLib) != EOF ) {

            handle = dlopen (nombreLib, RTLD_LAZY);

            getNombreFunciones (handle);

            dlclose (handle);
        }
    }


    printf ("Funciones de las librerias\n"
            "==========================\n\n");
    for (int i = 0; i < nomFun.pos; i++)
        printf ("%s\n", nomFun.nombreFun[i]);

    free (nomFun.nombreFun);

    return EXIT_SUCCESS;
}
