#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX 10

/*FUNCION
 * - 1 trozo de código que hace 1 cosa
 * - Tiene un nombre
 * - Recibe parametros o no
 * - Tiene valor de retorno o no
 * - Tiene 1/varias llamadas
 * - Posee{valor referencias}
*/

double eleva(double num, int pot){

    double result = 1;
    for(int i=0; i<pot; i++)
        result *= num;

    return result;
}

bool comprobar(int pot, double x, double d[MAX]){

    double result;

    if(pot == 2){
        printf(" |%.1lf %.1lf|-%.1lf ", d[0], d[1], x);
        result = d[0] + d[1]*x;
        if(result == 0){
            printf("%.3lf", d[0] + x * d[1]);
            return true;
        }
    }

    return false;
}

int main(){

    int potencia = 0, k=0;
    double result[] = {0,0,0,0,0,0}, c[MAX]={ 1,-1, 0, 0, 0, 0, 0, 0, 0, 0};
    bool comienzo = true;

    printf("Tu funcion es: ");
    for(int i = MAX-1; i >= 0; i--){
        if(c[i] == 0 && comienzo == true) continue;
        comienzo = false;

        potencia++;
        if(i == 0)
            printf(" %.lf", c[i]);
        else if(i == 1){
            printf(" %.lfX", c[i]);
            printf(" +");
        }
        else{
            printf(" %.lfX^%i", c[i], i);
            printf(" +");
        }
    }
    printf("\n\n");

    //Corte en el Eje X
    printf("Corte con el Eje X: ");
    for(double i=-MAX; i<MAX; i+=.5){
        bool end = false;
        if(potencia == 1){
            printf("%.2lf", c[0]);
            break;
        }
        if(potencia == 2)
            end = comprobar(potencia, i, c);
        if( end = true)break;
    }

    //Corte en el Eje Y
    printf("\nCorte con el Eje Y: %.lf\n", c[0]);


    return EXIT_SUCCESS;
}


