#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX 10
#define INC .05

double potencia(double x, int poten){

    double result = 1;

    for(int i=0; i<poten; i++)
        result *= x;

    return result;
}

int main(){

    double resultado, x, c[MAX] = { 0,-6, 0, 5, 0, 0, 0, 0, 0, 0};

    printf("Dime un punto y que te enseño sus puntos: ");
    scanf(" %lf", &x);

    for(double i=x; i<x+1; i+=INC){
        resultado = 0;
        for(int j=0; j<MAX; j++)
            //resultado += c[j] * pow(i, j);
            resultado += c[j] * potencia( i, j );

        /* Salida de Datos*/
        printf("f(%3.2lf) = %3.2lf\n", i, resultado);
    }

    return EXIT_SUCCESS;
}


