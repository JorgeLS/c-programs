#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

#define MAX 10

void titulo () {

    system ("clear");
    system ("toilet -fpagga -F crop 'CALCULADORA HP48'");
    printf("\n\n\t");
    system ("toilet -fpagga -F border '       NUMEROS:       '");
    printf("\n\n");
}

void imprimir (int p[MAX], int c) {

    for (int i = 0; i < c; i++)
        printf("\tNum %i.- %i\n", i+1, p[i]);
    printf("\n");
}

void push (int num, int p[MAX], int *c) {

    if (*c == 0 || *c % MAX != 0)
        p[(*c)++] = num;
    else
        fprintf(stderr, "No se pueden meter mas numeros quitar\n");
}

void pop (int p[MAX], int *c) {

    if (*c > 0)
        (*c)--;
    else
        fprintf(stderr, "No hay numeros que quitar\n");
}

void shift (int p[MAX], int *c) {

    if (*c > 0) {
        for (int i = 0; i < *c; i++)
            p[i] = p[i+1];
        (*c)--;
    }
    else
        fprintf(stderr, "La pila esta llena o vacia\n");
}

int main(int argc, char *argv[]){

    int pila[MAX];
    int cima = 0;
    int nuevo;
    bool fin;

    do {
        titulo();
        imprimir (pila, cima);

        printf ("\tDime un numero para meter en la pila: ");
        int d = scanf (" %i", &nuevo);
        __fpurge(stdin);

        if (d) {
            if (nuevo != 666)
                push (nuevo, pila, &cima);
            else
                shift (pila, &cima);
        }
        else
            pop (pila, &cima);

    }while(!fin);

    return EXIT_SUCCESS;
}
