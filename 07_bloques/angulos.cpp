#include <stdio.h>
#include <stdlib.h>
#include <math.h>

 

/*Dependiendo si GRADOS está definido K vale diferente*/
#ifdef GRADOS
#define K 1
const char *unidad = "º";
#else 
#define K M_PI / 180
const char *unidad = " rad"; 
#endif

double arad(double angulo){
#ifdef GRADOS
    return M_PI * angulo /180;
#else 
    return angulo;
#endif
}

int main(){
    for(double angulo=0; angulo<=K*360; angulo += K*.5){
        printf("%.2lf%s- %.4lf\n ", angulo, unidad, cos(arad(angulo)));
    }
    return EXIT_SUCCESS; 
}	


