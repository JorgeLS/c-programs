#include <stdio.h>
#include <stdlib.h>

int main()
{
    int a = 0, D;

    printf("Dime un número: ");
    scanf(" %d", &D);

    unsigned divi[D];

    for(int i = 2; i<D; i++)
        if(D %i == 0)
            divi[a++] = i;


    printf("Los divisores de %i son:", D);

    for(int i = 0; i<a; i++)
        printf(" %i",divi[i]);

    printf("\n");
    printf("El número %i tiene %li divisores\n",
           D,sizeof(divi)/sizeof(divi[0]));

    return EXIT_SUCCESS;
}


