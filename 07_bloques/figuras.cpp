#include <stdio.h>
#include <stdlib.h>

#define L 8
#define l 7
#define P printf("\n")
#define Ast printf("*")

int main(){

    //Triángulo rectángulo
    for(int f=0; f<L; f++){
        for(int c=0; c<=f; c++)
            Ast;
        P;
    }
    P;

    //Cuadrado
    for(int f=0; f<L; f++){
        for(int c=0; c<L; c++)
            Ast;
        P;
    }
    P;

    //Cuadrado Hueco
    for(int f=0; f<L; f++){
        for(int c=0; c<L; c++)
            if(f==0 || f == L-1 || c == 0 || c == L-1)
                Ast;
            else
                printf(" ");
        P;
    }
    P;

    //Cuadrado con Diagonal
    for(int f=0; f<L; f++){
        for(int c=0; c<L; c++)
            if(f == 0 || f == L-1 || c == 0 || c == L-1 || c == f)
                Ast;
            else
                printf(" ");
        P;
    }
    P;

    //Cuadrado con dos Diagonales
    for(int f=0; f<L; f++){
        for(int c=0; c<L; c++)
            if(f == 0 || f == L-1 || c == 0 || c == L-1 || c == f || L == c + f + 1)
                Ast;
            else
                printf(" ");
        P;
    }
    P;


    //Triángulo rectángulo inverso
    for(int f=L; f>0; f--){
        for(int c=0; c<f; c++)
            Ast;
        P;
    }

    P;

    //Rectángulo
    for(int f=0; f<L; f++){
        for(int c=0; c<l; c++)
            Ast;
        P;
    }

    P;

    //Triángulo Isósceles
    for(int f=0; f<L; f++){
        for(int c=0; c<=L-f; c++)
            printf("  ");
        for(int c=0; c<f; c++)
            printf("*   ");
        P;
    }

    P;

    //Rombo
    for(int f=0; f<L; f++){
        for(int c=0; c<=L-f; c++)
            printf("  ");
        for(int c=0; c<f; c++)
            printf("*   ");
        P;
    }

    for(int f=(L-2); f>0; f--){
        for(int c=0; c<=(L-f); c++)
            printf("  ");
        for(int c=0; c<f; c++)
            printf("*   ");
        P;
    }

    P;

    return EXIT_SUCCESS;
}


