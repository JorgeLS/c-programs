#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

// Parameters' struct
struct Tparms {
    char character;
    int count;
};

void* print (void* parameters) {

    // Call the parameters
    struct Tparms* p = (struct Tparms*) parameters;

    // Print the parameters
    for (int i = 0; i < p->count; i++) {
        // fprintf (stderr, "%i%c", i, p->character);
        fputc (p->character, stderr);
        // sleep (1);
    }

    return NULL;
}

int main (int argc, char* argv[]) {

    // Create the thread ID
    pthread_t thread1_id;
    pthread_t thread2_id;

    // Create the parameters
    Tparms thread1_args;
    Tparms thread2_args;

    thread1_args.character = 'x';
    thread1_args.count = 200;
    // thread1_args.count = 10;

    thread2_args.character = 'o';
    thread2_args.count = 300;
    // thread2_args.count = 15;

    // Create the threads for the parameters
    pthread_create (&thread1_id, NULL, &print, &thread1_args);
    pthread_create (&thread2_id, NULL, &print, &thread2_args);

    // Is like a wait in the process, make sure that the thread has finished
    // pthread_cancel (thread2_id);
    pthread_join (thread1_id, NULL);
    pthread_join (thread2_id, NULL);

    printf ("\n");

    return 0;
}
