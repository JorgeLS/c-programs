#include "stack.h"


int stack_error = STACK_NOERROR;

void
clear_error ()
{
    stack_error = STACK_NOERROR;
}

int stack_status ()
{
    return stack_error;
}

void
init (struct TStack *stack)
{
    stack->s= 0;
}

void
push (struct TStack *stack, pthread_t new_id)
{
    if (stack->s >= MAX_STACK)
    {
        stack_error = STACK_FULL;
        return;
    }

    clear_error ();
    stack->data[stack->s++] = new_id;
}

pthread_t pop (struct TStack *stack)
{
    if (stack->s <= 0)
    {
        stack_error = STACK_EMPTY;
        return (pthread_t) 0;
    }

    clear_error();
    return stack->data[--stack->s];
}
