#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>

#define FILENAME "salida.txt"

// Estructura de los Clientes
struct TClientes {
    int tiempo;
    pthread_t id;
    pthread_t salida;
};

// Estructura de la pila
struct TPila {
    struct TClientes *clientes;
    int cima = 1;
};

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void menu (int *op, struct TPila p) {

    // system ("clear");
    // system ("toilet -f pagga MARKET");
    printf ("\n");

    printf ("\n");
    printf ("1.- Crear un hilo/Entrar cliente\n");
    printf ("2.- Cepilla un hilo/Echar a un cliente\n");
    printf ("3.- Cerrar tienda\n");
    printf ("\n");

    for (int i = 0; i < p.cima; i++)
        printf("%lu\n", p.clientes[i].id);

    printf ("\nElige una opcion: ");
    scanf (" %i", op);
}

void* comprar (void* args) {

    struct TClientes *c = (struct TClientes *) args;

    // FILE *pf;
    // if ( !(pf = fopen(FILENAME, "a")) ) {
    //     fprintf (stderr, "No se ha abierto bien el archivo");
    //     exit(0);
    // }

    pthread_mutex_lock (&mutex);
    for (int i = 0; i <= c->tiempo; i++) {
        // fprintf (pf, "%2i .- %lu\n", i, c->id);
        // fflush (pf);
        fprintf (stderr, "%2i .- %lu\n", i, c->id);
        c->salida++;
        sleep(1);
    }
    pthread_mutex_unlock (&mutex);

    return NULL;
}

void pushCliente (struct TPila *p) {

    pthread_t pthread_id;

    p->clientes = (struct TClientes *) realloc (p->clientes, (p->cima + 1) * sizeof(struct TClientes) );
    p->clientes[p->cima].tiempo = 10;

    printf("%i\n", pthread_create (&p->clientes[p->cima].id, NULL, &comprar, &p->clientes[p->cima]));

    p->cima++;
}

void popCliente (struct TPila *p) {

    if (p->cima != 0) {
        p->cima--;
        // pthread_cancel (p->clientes[p->cima].id);
        // pthread_exit ();
        // pthread_join (p->clientes[p->cima].id, NULL);
        p->clientes = (struct TClientes *) realloc (p->clientes, (p->cima + 1) * sizeof(struct TClientes) );
    }
}

int main (int argc, char* argv[]) {

    struct TPila pila;
    int op = 0;

    bzero (&pila, sizeof(pila));
    system ("rm " FILENAME);
    system ("touch " FILENAME);

    pila.clientes = (struct TClientes *) malloc ((pila.cima + 1) * sizeof(struct TClientes));

    do
    {
        menu (&op, pila);

        switch (op)
        {
            case 1:
                pushCliente (&pila);
                break;
            case 2:
                popCliente (&pila);
                break;
            case 3:
                printf ("Cerramos nuestras puertas, clientes vayan saliendo.\n");
                sleep(1);
                break;
            default:
                printf ("Opcion no valida.\n");
                break;
        }
    } while ( op != 3 );

    for (int i = 0; i < pila.cima; i++) {
        pthread_join (pila.clientes[i].id, NULL);
    }

    free (pila.clientes);

    printf ("Cima: %i\n", pila.cima);

    return EXIT_SUCCESS;
}
