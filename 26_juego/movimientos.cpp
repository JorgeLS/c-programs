#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <time.h>

#define KLENTO 0.005
#define KRAPID (2 * KLENTO)
#define MAX 0x10 // Limite de pesonajes
#define LIM 0x64 // Limite del mapa

// (TipoVector) Variables de los vectores de posicion y velocidad
struct TVector {
    double x;
    double y;
};

// Esctructura de cada objeto movil
struct TMovil {
    struct TVector pos;
    struct TVector vel;
    void (*move) (struct TMovil *p);
};

// Pila
// La pila contendrá todos los objetos o personajess movibles
struct TPila {
    struct TMovil *data[MAX];
    int cima;
};

// Funciones de la Pila
void push (struct TPila *obj, struct TMovil *n) {
    if (obj->cima >= MAX)
        return;
    obj->data[obj->cima++] = n;
}

// Movements
void move_slow (struct TMovil *obj) {
    obj->pos.x += obj->vel.x * KLENTO;
    obj->pos.y += obj->vel.y * KLENTO;
}

void move_fast (struct TMovil *obj) {
    obj->pos.x += obj->vel.x * KRAPID;
    obj->pos.y += obj->vel.y * KRAPID;
}

void iniciar (struct TMovil *obj) {

    obj->pos.x = rand() % 100;
    obj->pos.y = rand() % 100;
    obj->vel.x = rand() % 20;
    obj->vel.y = rand() % 20;
}

int main(int argc, char *argv[]){

    srand (time(NULL));

    // Inicializamos los personajess
    struct TPila personajes;
    personajes.cima = 0;
    int num_personajes = 0;
    // Inicializamos variables
    bool end = false;
    int j, num = 0;

    // Pregunta cuantos personajes
    do {

        printf ("Cuantos personajes quieres?: ");
        j = scanf (" %i", &num_personajes);
        __fpurge (stdin);

        if (j && num_personajes <= MAX)
            break;
        else
            printf ("Eso no me vale dime otro numero\n\n");

    } while (1);

    // Creando los personajes
    do {
        system("clear");

        struct TMovil *nuevo = (struct TMovil *) malloc (sizeof(struct TMovil));
        iniciar (nuevo);
        push(&personajes, nuevo);

        /* Datos */
        printf ("\n");
        for (int i = 0; i < personajes.cima; i++)
            printf ("\tPersonaje %i: (X: %.2lf, Y: %.2lf)\n", i,
                    personajes.data[i]->pos.x,  personajes.data[i]->pos.y);

        if (personajes.cima == num_personajes) break;

    } while (1);

    printf("\n");

    // Moviendo personajes
    do {
        int d;
        if (num > 0) {
            num--;
            printf ("%i\n", num);
            continue;
        }
        else {
          d = scanf (" %i %i", &num, &j);
          __fpurge(stdin);
        }

        system ("clear");
        if (d)
            for (int i = 0; i < personajes.cima; i++)
                move_fast (personajes.data[i]);
        else
            for (int i = 0; i < personajes.cima; i++)
                move_slow (personajes.data[i]);

        printf ("\n");
        for (int i = 0; i < personajes.cima; i++)
            printf ("\tPersonaje %i: (X: %.2lf, Y: %.2lf)\n", i,
                    personajes.data[i]->pos.x,  personajes.data[i]->pos.y);
    } while (!end);

    for (int i = 0; i < personajes.cima; i++)
        free(personajes.data[i]);

    return EXIT_SUCCESS;
}
