#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <strings.h>

#define MAX 0x16

#define RED   "\x1B[31m"
#define CYAN  "\x1B[36m"
#define RESET "\x1B[0m"

struct TCola {
    int data[MAX];
    int base;
    int cima;
};

void push (struct TCola *cola, int dato) {

    if (cola->cima - cola->base < MAX)
        cola->data[cola->cima++ % MAX] = dato;
}

void shift (struct TCola *cola) {

    if (cola->cima - cola->base > 0)
        cola->data[cola->base++];
}

void imprimir (struct TCola c){
    printf ("\tLA COLA\n");
    printf ("\t=======\n\n");
    for (int i=c.base; i<c.cima; i++)
        printf ("\t   %i\n", c.data[i % MAX]);
    printf ("\n");
}


void titulo () {
    system ("clear");
    system ("toilet -f pagga ' COLA '");
    printf ("\n");
}

int  main(int argc, char *argv[]){

    struct TCola cola;
    int nuevo, leidos;
    bool fin = false;
    bzero (&cola, sizeof (cola));

    do {
        titulo ();
        imprimir(cola);

        leidos = scanf (" %i", &nuevo);
        __fpurge(stdin);

        if (leidos)
            push (&cola, nuevo);
        else
            shift (&cola);

    } while (!fin);

    return EXIT_SUCCESS;
}
