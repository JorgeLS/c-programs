#include <stdio.h>
#include <stdlib.h>

#define DIM 3
#define nVec 2
#define X 0
#define Y 1
#define Z 2

int main(){
    double vec[DIM][DIM],
    DetermSuma[DIM] = {1,1,1},
    DetermResta[DIM] = {1,1,1},
    determinante;

    for(int i=0; i<3; i++){
        printf("Dime la fila %i: ", i);
        scanf(" %lf, %lf, %lf", &vec[i][0], &vec[i][1], &vec[i][2]);
    }

    /* DETERMINANE */
    /* SUMA */
    for(int i=0; i<DIM; i++){
        for(int j=0; j<DIM; j++)
            /* Esto se puede resumir
               int f=j+i;
               if(f > 2)
               f=j+i-3;
               DetermSuma[i] *= vec[f][j];*/
            DetermSuma[i] *= vec[(j+i) %DIM][j];
        determinante += DetermSuma[i];
    }
    /* RESTA */
    for(int i=0; i<DIM; i++){
        for(int j=DIM-1; j>=0; j--)
            DetermResta[i] *= vec[(2-j+i)%DIM][j];
        determinante -= DetermResta[i];
    }

    printf("El determinante de la matriz introducida es %.2lf\n", determinante);

    return EXIT_SUCCESS;
}

