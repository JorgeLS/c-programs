#include <stdio.h>
#include <stdlib.h>

int main(){

    int DIMf = 40,
        DIMc = 40,
        e, E = 2;

    /* Entrada de Datos */

    printf("A cuanto quieres elevar tu matriz: ");
    scanf(" %d", &e);

    printf("Dime como es tu matriz: ");
    scanf(" %d %d", &DIMf, &DIMc);

    /* Inicialización de Datos */

    double matriz1[DIMf][DIMc];
    double matriz2[DIMf][DIMc],
    result[DIMf][DIMc];

    for(int f=0; f<DIMf; f++){
        printf("Dime la fila Nº%i: ", f+1);
        for(int c=0; c<DIMc; c++)
            scanf(" %lf", &matriz1[f][c]);
    }

    for(int f=0; f<DIMf; f++)
        for(int c=0; c<DIMc; c++)
            matriz2[f][c] = matriz1[f][c];

    /* Calculo de la elevación */

    if(e < 0){
        printf("\nNo te puedo hacer esa operación\n\n");
        return 0;
    }

    else if(e == 0){
        for(int f=0; f<DIMf; f++){
            for(int c=0; c<DIMc; c++){
                if(c == f)
                    result[f][c] = 1;
                else
                    result[f][c] = 0;
            }
        }
    }

    else if(e == 1)
        for(int f=0; f<DIMf; f++)
            for(int c=0; c<DIMc; c++)
                result[f][c] = matriz1[f][c];

    else{
        do{
            if(E != 2)
                for(int f=0; f<DIMf; f++)
                    for(int c=0; c<DIMc; c++)
                        matriz2[f][c] = result[f][c];

            for(int f=0; f<DIMf; f++)
                for(int c=0; c<DIMc; c++)
                    result[f][c] = 0;

            for(int i=0; i<DIMf; i++){
                for(int j=0; j<DIMc; j++)
                    for(int z=0; z<DIMf; z++)
                        result[i][j] += matriz2[i][z] * matriz1[z][j];
            }
            E++;
        }while(E <= e);
    }

    /* Salida De Datos */

    printf("\n");

    for(int f=0; f<DIMf; f++){
        printf("\t");
        for(int c=0; c<DIMc; c++)
            printf("%3.lf ", result[f][c]);
        printf("\n");
    }

    printf("\n");

    return EXIT_SUCCESS;
}


