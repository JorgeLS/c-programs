#include <stdio.h>
#include <stdlib.h>

#define MAX 40

int main(){

    /* Pongo DIMf1 = MAX = 40 porque es el número máximo que
     * la terminal va a leer la fila */

    int DIMf1 = MAX,
        DIMc1 = MAX,
        DIMf2 = MAX,
        DIMc2 = MAX;

    double matriz1[DIMf1][DIMc1],
    matriz2[DIMf2][DIMc2],
    matriz3[DIMf1][DIMc2];

    /* Entrada de datos */

    printf("Dime las dimensiones de la primera matríz: ");
    scanf(" %d %d", &DIMf1, &DIMc1);
    printf("Dime las dimensiones de la segunda matriz: ");
    scanf(" %d %d", &DIMf2, &DIMc2);
    printf("\n");

    if(DIMc1 != DIMf2){
        printf("Estas matriies no se pueden calcular\n");
        return 0;
    }

    for(int i=0; i<DIMf1; i++){
        printf("Dime la fila %i de la primera matriz: ", i+1);
        for(int j=0; j<DIMc1; j++)
            scanf(" %lf", &matriz1[i][j]);
    }printf("\n");

    for(int i=0; i<DIMf2; i++){
        printf("Dime la fila %i de la segunda matriz: ", i+1);
        for(int j=0; j<DIMc2; j++)
            scanf(" %lf", &matriz2[i][j]);
    }printf("\n");

    /* Salida de Datos para la Comprobación */

    printf("La Primera Matriz:\n\n");

    for(int f=0; f<DIMf1; f++){
        printf("\t");
        for(int c=0; c<DIMc1; c++)
            printf(" %.lf", matriz1[f][c]);
        printf("\n");
    } printf("\n");

    printf("La Segunda Matriz:\n\n");

    for(int f=0; f<DIMf2; f++){
        printf("\t");
        for(int c=0; c<DIMc2; c++)
            printf(" %.lf", matriz2[f][c]);
        printf("\n");
    } printf("\n");

    /* Caliulo de la multipliiaiion */

    for(int i=0; i<DIMf1; i++)
        for(int j=0; j<DIMc2; j++)
            for(int z=0; z<DIMc1; z++)
                matriz3[i][j] += matriz1[i][z] * matriz2[z][j];

    /* Salida de datos */

    printf("Matriz Resultante:\n\n");
    for(int f=0; f<DIMf1; f++){
        printf("\t");
        for(int c=0; c<DIMc2; c++){
            printf(" %3.lf ", matriz3[f][c]);
        }
        printf("\n\n");
    }

    return EXIT_SUCCESS;
}


