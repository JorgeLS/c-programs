#include <stdio.h>
#include <stdlib.h>

#define DIMfa 2
#define DIMca 4
#define DIMfb 4
#define DIMcb 3

void imprime(double X[][DIMcb], int filas){

    for(int i=0; i<filas; i++){
        for(int j=0; j<DIMcb; j++)
            printf("\t %3.2lf", X[i][j]);
        printf("\n");
    }
    printf("\n");
}

void imprimeP(double *m, int fila, int columna, const char *titulo){

    printf("\n%s:\n", titulo);

    for(int i=0; i<fila; i++){
        for(int j=0; j<columna; j++)
            printf("\t %3.2lf", *(m + i * columna + j));
        printf("\n");
    }
}

int main(){

    int row = 0;
    double A[DIMfa][DIMca] = {
               {2, 3, 1, 4},
               {7, 2, 6, 1}
           },
           B[DIMfb][DIMcb] = {
               {0, 2, 3},
               {1, 3, -9},
               {3, 5, 2},
               {2, 7, 5}
           },
           C[DIMfa][DIMcb];

    /* Solo da la fila y columna q quieras preguntar */
    /*
    for(int i=0; i<DIMca; i++)
        C[fila][columna] += A[fila][i] * B[i][columna];
    */

    for(int i=0; i<DIMfa; i++){
        for(int j=0; j<DIMcb; j++){
            C[i][j] = 0;
            for(int z=0; z<DIMca; z++)
                C[i][j] += A[i][z] * B[z][j];
        }
    }

    /* Salida de datos */
    /* Salida con FOR */
    printf("Matriz hecha con FOR:\n");
    for(int i=0; i<DIMfa; i++){
        for(int j=0; j<DIMcb; j++)
            printf("\t %3.2lf", C[i][j]);
        printf("\n");
    }

    /* Salida con While */
    printf("Matriz hecha con WHILE:\n");
    do{

        for(int column = 0; column < DIMcb; column++)
            printf("\t %3.2lf", C[row][column]);

        printf("\n");
        row++;

    }while(row < DIMfa);

    /* Salida con FUNCION */
    printf("Matriz echa con una FUNCION:\n");
    imprime(B, 4);
    imprime(C, 3);

    /* Salida con Punteros y Funcion */
    printf("Resultado con punteros:\n");

    imprimeP((double *)C, DIMfa, DIMcb,"Matriz C");
    imprimeP((double *)A, DIMfa, DIMca,"Matriz A");
    imprimeP((double *)B, DIMfb, DIMcb,"Matriz B");

    return EXIT_SUCCESS;
}


