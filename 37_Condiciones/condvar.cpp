#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

int thread_flag;
pthread_cond_t thread_flag_cv;
pthread_mutex_t thread_flag_mutex;

void initialize_flag ()
{
	pthread_mutex_init (&thread_flag_mutex, NULL);
	pthread_cond_init (&thread_flag_cv, NULL);

	thread_flag = 0;
}

void do_work ()
{
	printf ("Hello, number %i\n", thread_flag);
}

void set_thread_flag (int flag_value)
{
	pthread_mutex_lock (&thread_flag_mutex);
	thread_flag = flag_value;
	pthread_cond_signal (&thread_flag_cv);
	pthread_mutex_unlock (&thread_flag_mutex);
}

void inc_thread_flag ()
{
	pthread_mutex_lock (&thread_flag_mutex);
	thread_flag++;
	pthread_cond_signal (&thread_flag_cv);
	pthread_mutex_unlock (&thread_flag_mutex);
}

void* thread_function (void* thread_arg)
{
	while (1)

	{
		pthread_mutex_lock (&thread_flag_mutex);
		// Cuando thread_flag sea igual 0 se ejecutara la espera
		// When thread_flag is equal 0 the wait will be executed
		//while (!thread_flag)
		while (!(thread_flag % 2))
			pthread_cond_wait (&thread_flag_cv, &thread_flag_mutex);
		pthread_mutex_unlock (&thread_flag_mutex);

		do_work ();

		usleep (70000);
	}

	return NULL;
}

int main ()
{
	initialize_flag ();

	pthread_t thread_id1;

	pthread_create (&thread_id1, NULL, &thread_function, NULL);

	for (int i = 0; i < 100; i++)
	{	
		// Si no se pone el 1, la funcion de do_work no se ejecutará,
		// ya que el hilo espera a que un cambio en thread_flag
		// If you don't put the 1, the function do_work doesn't execute
		// because the thread wait a change in thread_flag
		//set_thread_flag (i % 2);
		inc_thread_flag ();
		sleep (1);
	}
	pthread_join (thread_id1, NULL);

	return 0;
}
