#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

#define OFF			0
#define ON			1
#define RACE_DIS	35

#define CLRSCR 		printf ("\x1B[2J"); //printf ("\033[2J");
#define NEWLINE 	printf ("\n\n\n");
#define CURSAVE		printf ("\x1B[2J"); //printf ("\033[S");
#define CURRES		printf ("\x1B[2J"); //printf ("\033[U");
#define COL(c,f) 	printf ("\x1B[2J"); //printf ("\033[%i;%iH", (c), (f));

int thread_flag;
int posRunner;
int h, m, s;
pthread_cond_t thread_flag_cv;
pthread_mutex_t thread_flag_mutex;

void initialize_flag ()
{
	pthread_mutex_init (&thread_flag_mutex, NULL);
	pthread_cond_init (&thread_flag_cv, NULL);

	srand( time(NULL) );

	thread_flag = 0;
	h = 0;
	m = 0;
	s = 0;
	posRunner = 0;
}

void set_thread_flag (int flag_value)
{
	pthread_mutex_lock (&thread_flag_mutex);
	thread_flag = flag_value;
	pthread_cond_signal (&thread_flag_cv);
	pthread_mutex_unlock (&thread_flag_mutex);
}

void do_work ()
{
	COL (20, 5)
	printf ("Han pasado %i segundos\n", s % 60);

	set_thread_flag (OFF);
}

void* thread_function (void* thread_arg)
{
	while (1)
	{
		pthread_mutex_lock (&thread_flag_mutex);
		while (!thread_flag)
			pthread_cond_wait (&thread_flag_cv, &thread_flag_mutex);
		pthread_mutex_unlock (&thread_flag_mutex);

		do_work ();

		if (posRunner >= RACE_DIS)
			break;
	}

	return NULL;
}

void print_clock ()
{
	COL (6, 26)
	printf ("RELOJ");
	COL (9, 20)
	printf ("----------------");
	COL (10, 20)
	printf ("\033[4M| %02i : %02i : %02i |\033[0M", h, m, s % 60);
	COL (11, 20)
	printf ("----------------");
	fflush (stdout);
}

void print_runner () 
{
	for (int i = 0; i <= RACE_DIS; i++)
	{
		COL (15, 10 + i)
		if (i == 0 || i == RACE_DIS)
			printf("|");
		else
		{
			if (i == posRunner + 1)
				printf ("¥");
			else
				printf ("_");
		}
	}
}

void move_runner ()
{
	posRunner += 1;// rand() % 2 + 1;
}

void* clock_function (void* thread_arg)
{
	while (1)
	{
		CLRSCR
		print_runner ();
		print_clock ();

		if (s % 5 == 0)
		{
			set_thread_flag (ON);
			move_runner ();
			if (posRunner >= RACE_DIS)
				break;
		}

		sleep (0);

		pthread_mutex_lock (&thread_flag_mutex);
		s++;

		if (s % 60 == 0)
		{
			m++;
			if (m == 60)
			{
				m = 0;
				h++;
			}
		}
		pthread_mutex_unlock (&thread_flag_mutex);
	}

	return NULL;
}

int main ()
{
	set_thread_flag (0);

	pthread_t thread_id;
	pthread_t clock;

	pthread_create (&thread_id, NULL, &thread_function, NULL);
	pthread_create (&clock, NULL, &clock_function, NULL);

	pthread_join (thread_id, NULL);
	pthread_join (clock, NULL);

	return 0;
}