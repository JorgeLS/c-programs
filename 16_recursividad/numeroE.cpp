#include <stdio.h>
#include <stdlib.h>

double factorial_de (int num){

    if(num == 0)
        return 1;

    return num * factorial_de (num-1);
}

double calc(int repes){

    double result = 0;

    for(int i=0; i<repes; i++)
        result += 1 / factorial_de(i);

    return result;
}
int main(int argc, char *argv[]){

    int num = 20;

    if(argc > 1)
        num = atoi( argv[1] );

    double e = calc(num);

    printf("\nEl numero 𝑒 = %.20lf\n\n", e);
    return EXIT_SUCCESS;
}


