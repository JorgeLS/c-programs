#include <stdio.h>
#include <stdlib.h>

void imprime_todo(char *frase){
    if(*frase == '\0')
        printf(".");
    else{
        printf("%c", *frase);
        imprime_todo(frase+1);
    }
}

void imprime_todo_alreves(char *frase){
    if(*frase == '\0')
        printf(".");
    else{
        imprime_todo_alreves(frase+1);
        printf("%c", *frase);
    }
}

int main(int argc, char *argv[]){

    char frase[]="Dabale arroz a la zona del abad";//31 + \0

    int t [] = {0,0,1};

    char *f = frase;

    printf("Imprime 1 a 1 con for.\n");
    for(int i=0; i<sizeof(frase); i++)
        printf("%c", f[i]);
    printf(".\n\n");

    printf("Imprime un frase con función recursiva y después al reves:\n");
    imprime_todo(f);
    printf("\n");
    imprime_todo_alreves(f);
    printf("\n");

    return EXIT_SUCCESS;
}


