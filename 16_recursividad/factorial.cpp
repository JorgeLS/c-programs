#include <stdio.h>
#include <stdlib.h>

int factorial (int result){

    if(result == 0)
        return 1;
    return result * factorial( result-1 );
}
int main(int argc, char *argv[]){

    long int n = atoi( argv[1] );

    n = factorial(n);

    printf("El factorial de %i es %li\n", atoi( argv[1] ), n);

    return EXIT_SUCCESS;
}


