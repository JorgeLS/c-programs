#include <stdio.h>
#include <stdlib.h>

double frac (int num, int levels){

    levels--;
    if(levels == 0)
        return num;

    return num + (1 / frac ( num, levels) );
}

int main(int argc, char *argv[]){

    int num = atoi( argv[1] );
    int niveles = atoi( argv[2] );

    double fraccion = frac( num, niveles );

    printf("Tu numero es %.6lf\n", fraccion);

    return EXIT_SUCCESS;
}


