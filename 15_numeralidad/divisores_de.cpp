#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX 0x100 //0x100 es 256 //Constantes Simbólicas
//En el define no puedes poner saltos de lineas pero si pones \ te vale
//EJ: #define MAX if(num == 0) \
{                            \
    printf("hola");          \
}
//#define INFORMA printf
//#define INFORMA(x) printf("Me han dado el numero %i", (x)); //Macros
//Funciones que aceptan un numero variable de argumentos son variadic function
//Macro Variadic:
#define INFORMA(...) if (verbose_flag) \
{                                      \
    printf(__VA_ARGS__);               \
    printf("\n");                      \
}

bool help_flag   = false,
    verbose_flag = false;
const char *program_name;

void print_usage(FILE *pf, int exit_code){

    fprintf(pf, "%s lo que sea\n", program_name);
    exit (exit_code);
}

int divi(int num, int divisores[MAX]){

    int n = 0;
    //Calculo de divisores
    for(int d=1; d<num; d++)
        if(num % d == 0)
            divisores[n++] = d;

    return n;
}

int main(int argc, char *argv[]){

    if(argc < 2)
        return EXIT_FAILURE;

    program_name = argv[0];
    int o;

    while( (o = getopt(argc, argv, "hv")) != -1 ){

        switch (o){
            case 'h':
                help_flag = true;
                break;

            case 'v':
                verbose_flag = true;
                break;

            case '?':
                print_usage (stderr, 1);
                break;
        }
    }

    INFORMA ("Voy a cojer el parametro de la terminal.")
    int num = atoi(argv[optind]), num_divi = 0; //optind analiza el parametro hasta que o es ?
    INFORMA ("He recogido el parametro %i de la terminal", num)

    int divisores[MAX];
        if (help_flag)
        print_usage (stdout, 0);

    INFORMA ("Voy a calcular los divisores.")
    //Calculo de divisores
    num_divi = divi(num, divisores);
    INFORMA ("He encontrado %i divisores", num_divi)


    //Salida de Datos
    printf("\nSus divisores son:\n");
    for(int i=0; i<num_divi; i++)
        printf("\t%i\n", divisores[i]);

    printf("\n");
    return EXIT_SUCCESS;
}

