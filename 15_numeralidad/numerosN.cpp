#include <stdio.h>
#include <stdlib.h>

#define DIM 10

int cuadrado(int *num1){

    *num1 *= *num1;
}

void rellena(int A[DIM]){

    for(int i=0; i<DIM; i++){
        A[i] = i;
        cuadrado(&A[i]);
    }
}

int main(){

    int num[DIM];

    rellena(num);

    for(int i=0; i<DIM; i++)
        printf("Tu cuadrado de %i = %i\n", i, num[i]);

    return EXIT_SUCCESS;
}


