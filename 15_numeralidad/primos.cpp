#include <stdio.h>
#include <stdlib.h>

#define NUM 1000000

char const *program_name;
//char const *program_name asi la dirección de memoria puede cambiar
//char * const program_name asi la dirección no puede cambiar y se la tienes que dar

void print_usage(int exit_code){

    FILE *f = stdout; //FILE * señala a un stream
    if(exit_code != 0)
        f = stderr;

    fprintf(f,
            " - Checks for primaly of the argument. -\n\
            Usage:  %s <number>\n\n\
            number: Positive integer.\n\
            \n", program_name);
    exit(exit_code);//Otra forma de terminar el programa con exit_code como el valor
}

int pregunta_num(){
    int num;

    printf("Dime un numero: ");
    scanf(" %i", &num);

    if(num <= 0){
        printf("Este numero no me vale\n");
        pregunta_num();
    }
    else if(num == 1){
        printf("El uno es primo siempre con sigo mismo\n");
        pregunta_num();
    }
    else
        return num;
}

bool primo(int num){
    for(int d=num; d>1; d--)
        if(num % d == 0 && num != d)
            return false;
    return true;
}

int main(int argc, char *argv[]){

    /*
     * ./saluda juan pepe --> el programa lo lee por tokens y entre media \0
     * ./saluda\0juan\0pepe\0
     * argv apunta a cada token que es 0=./saluda, 1=juan, 2=pepe
     * y argc cuenta los \0 que hay
     *
     * atoi --> ASCII to INT
     * */

    program_name = argv[0];

    if(argc < 2)
        print_usage (1);

    //int num = pregunta_num();
    int num = atoi(argv[1]);

    printf("%s es primo el %i.\n",
            primo(num) ? "Sí" : "No", num);

    printf("argc = %i\n", argc);
    for(int i=1; i<argc; i++){
       printf("argv[%i] = %i\n", i, atoi(argv[i]));
    }

    return EXIT_SUCCESS;
}


