#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX 0x100

int DIVI (int num, int divisores[MAX]){

    int n_divi = 0;
    for(int d=1; d<num; d++)
        if(num % d == 0)
            divisores[n_divi++] = d;

    return n_divi;
}

int SUMA (int num_divi, int list[MAX]){

    int suma = 0;
    for(int i=0; i<num_divi; i++)
        suma += list[i];

    return suma;
}

int main(int argc, char *argv[]){

    int num = atoi(argv[optind]); //optind = 1
    int divisores[MAX];

    //Calculo de divisores
    //Calculo de los divisores
    int num_divi = DIVI (num, divisores);

    //Calculo de la suma de los divisores
    int suma_divi = SUMA (num_divi, divisores);

    //Salida de Datos
    if(suma_divi == num)
        printf("El %i es un numero Perfecto\n", num);
    else
        printf("El %i no es un numero Perfecto\n", num);

    return EXIT_SUCCESS;
}

