#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>

#define MAX 10

char buf [24];
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void* imprime (void* args) {

    sem_t count = *(sem_t*) args;

    sem_wait (&count);
    pthread_mutex_lock (&mutex);

    // sprintf (buf, "%s", "Caracola\n");
    printf ("a + %lu\n", pthread_self());

    pthread_mutex_unlock (&mutex);
    sem_post (&count);

    return NULL;
}

int main (int argc, char* argv[]) {

    pthread_t pt_id1;
    pthread_t pt_id2;
    sem_t sem_id;
    char b [24];

    sem_init (&sem_id, 0, 1);

    pthread_create (&pt_id1, NULL, &imprime, &sem_id);
    pthread_create (&pt_id2, NULL, &imprime, &sem_id);

    pthread_join (pt_id1, NULL);
    pthread_join (pt_id2, NULL);

    printf ("hola - %s", buf);

    sem_destroy (&sem_id);

    return EXIT_SUCCESS;
}

// sem_wait espera en el hilo
// sem_pos atender y sacarle del wait
