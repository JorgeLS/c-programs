#include <stdio.h>
#include <stdlib.h> 
#include <pthread.h> 
#include <math.h> 
#include <semaphore.h>
#include <time.h>
#include <unistd.h>

#define MAX 200

int count;

sem_t sem_mutex;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void* thread1(void* arg) 
{
	while (count != 200) {

	    int sumar = rand() % 2 + 1;

    	printf ("+%i\n", sumar);
	    	
		for (int i = 0; i < sumar; i++) {

		    sem_wait(&sem_mutex);
			
			pthread_mutex_lock (&mutex);

		    printf ("Entered thread1 %lu %i\n", pthread_self(), count++);

			pthread_mutex_unlock (&mutex);
		}
	}

    usleep(10000);
} 

void* thread2(void* arg) 
{
	while (count != 200) {

	    int sumar = rand() % 2 + 1;
    	
    	printf ("+%i\n", sumar);

	    for (int i = 0; i < sumar; i++) {

		    sem_wait(&sem_mutex);

			pthread_mutex_lock (&mutex);

		    printf ("Entered thread2 %lu %i\n", pthread_self(), count++);

			pthread_mutex_unlock (&mutex);
		}

	    usleep(50000);
	}
} 

void* capataz (void* arg) {

	for (int i = 0; i < MAX; i++) {
	    pthread_mutex_lock (&mutex);
		
		sem_post(&sem_mutex);
	    
	    pthread_mutex_unlock (&mutex);

		usleep (100000);
	}
}
int main() 
{
    sem_init(&sem_mutex, 0, 1);

    srand (time(NULL));

    count = 0;

    pthread_t t1;
    pthread_t t2;
    pthread_t t3;

    pthread_create(&t1, NULL, capataz, NULL);
    pthread_create(&t2, NULL, thread1, NULL);
    pthread_create(&t3, NULL, thread2, NULL);

    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    pthread_join(t3, NULL);

    sem_destroy(&sem_mutex);

    return 0;
} 