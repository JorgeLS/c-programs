#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ncurses.h>

#include "snake.h"

void pintar (struct TSnake snake, struct TVector fruit) {

    // Borra
    clear ();

    for (int i = 0; i < COLS; i++)
        mvprintw (2, i, "-");

    mvprintw (1, LIM_MENU, "Puntos: %i - P %i %i -- F %.2lf %.2lf"
                " -- S %.2lf %.2lf -- V %i",
                puntos, LINES, COLS,
                fruit.y, fruit.x,
                snake.anillo[0].pos.y, snake.anillo[0].pos.x,
                snake.vidas);
    // Snake and berry
    for (int i = 0; i < snake.cima; i++)
        mvprintw ( snake.anillo[i].pos.y,
                   snake.anillo[i].pos.x,
                   "O");
    mvprintw (fruit.y, fruit.x, "G");
    refresh ();
}

bool volverAtras (struct TSnake snake, int tecla) {
    if (snake.anillo[0].pos.x + velocidades[tecla].x == snake.anillo[1].pos.x)
        return true;
    if (snake.anillo[0].pos.y + velocidades[tecla].y == snake.anillo[1].pos.y)
        return true;
    return false;
}

void choque (struct TSnake *snake) {

}

int main(int argc, char *argv[]) {

    struct TSnake snake;
    struct TVector berry;
    int input;
    bool end = false;

    // Iniciamos la pantalla
    initscr ();

    halfdelay (2);
    keypad (stdscr, TRUE);
    curs_set(FALSE);

    iniciar (LINES, COLS);

    parir (&snake);
    posicionar_berry (&berry);

    snake.vidas = 3;

    do {
        // Mirar entradas
        input = getch ();
        if ( input >= KEY_DOWN && input <= KEY_RIGHT && !volverAtras(snake, input - KEY_DOWN) ) {
            snake.anillo[0].vel = velocidades [input - KEY_DOWN];
        }
        choque (&snake);
        // Solo movemos la cabeza
        // sentido (&snake, input);
        // Actualizar físicas
        actualizarFisicas (&snake);
        mover (&snake);
        comer (&snake, &berry);
        // Repintar
        pintar (snake, berry);

    } while (input != 0x1B && snake.vidas != 0);
    //} while (snake.vidas != 0);

    // Cerramos la pantalla
    curs_set (TRUE);
    endwin ();

    free (snake.anillo);

    return EXIT_SUCCESS;
}
