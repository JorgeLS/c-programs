#ifndef __SNAKE_H__
#define __SNAKE_H__

#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ncurses.h>

#define AMAX 0x200
#define POSIB 5
#define LIM_MENU 3

struct TVector {
    double x;
    double y;
};

struct TAnillo {
    struct TVector pos;
    struct TVector vel;
};

struct TSnake {
    struct TAnillo *anillo;
    int cima;
    int vidas;
};

extern int puntos;
extern const struct TVector velocidades[POSIB];

// Para que lo reconozca tanto en C como en C++
#ifdef __cplusplus
extern "C" {
#endif

  void iniciar            (int lines, int cols);
  void parir              (struct TSnake *cabeza);
  void posicionar_berry   (struct TVector *fruit);
  void comer              (struct TSnake *snake, struct TVector *fruit);
  bool actualizarFisicas  (struct TSnake *snake);
  void mover              (struct TSnake *snake);
  void sentido            (struct TSnake *snake, int tecla);
  void pintar             (struct TSnake snake, struct TVector fruit);
  void crecer             (struct TSnake *snake);

#ifdef __cplusplus
}
#endif

#endif
