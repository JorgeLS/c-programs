#include "snake.h"

#define AINI 5
#define INI 0

enum direcciones {
    quieto,
    arriba,
    abajo,
    izquierda,
    derecha
};

int puntos = 0;
int iniciado = 0;
int max_x = 80,
    max_y = 25;

const struct TVector velocidades[POSIB] = {
    { 0,  1}, // Abajo
    { 0, -1}, // Arriba
    {-1,  0}, // Izquierda
    { 1,  0}, // Derecha
    { 0,  0}  // Quieto
};

void iniciar (int lines, int cols) {
    iniciado = 1;
    puntos = 0;
    srand (time (NULL));

    max_x = cols;
    max_y = lines;
}

void parir (struct TSnake *snake) {
    bzero (snake, sizeof (struct TSnake));
    // Inicializamos la cabeza del snake
    snake->anillo = (struct TAnillo *) realloc (snake->anillo, (snake->cima + 1) * sizeof (TAnillo));

    snake->anillo[0].pos.x = rand () % max_x;
    snake->anillo[0].pos.y = rand () % (max_y - LIM_MENU) + LIM_MENU;

    // Damos un valor tanto a vel.x como a vel.y
    snake->anillo[0].vel = velocidades [rand () % POSIB];
    snake->cima++;

    for (int i = 1; i < AINI; i++)
        crecer(snake);
}

void posicionar_berry (struct TVector *fruit) {
    fruit->x = rand () % max_x;
    fruit->y = rand () % (max_y - LIM_MENU) + LIM_MENU;
}

bool actualizarFisicas (struct TSnake *snake) {
    int vida = snake->vidas;
    if (
            (snake->anillo[0].pos.x >= 0 && snake->anillo[0].pos.x < COLS) &&
            (snake->anillo[0].pos.y >= 3 && snake->anillo[0].pos.y < LINES)
        )
        return false;
    else {
        parir (snake);
        vida--;
        snake->vidas = vida;
        return true;
    }

    return false;
}

void sentido (struct TSnake *snake, int tecla) {

    switch (tecla) {
        case KEY_UP:
            if (snake->anillo[0].pos.y - 1 != snake->anillo[1].pos.y)
                snake->anillo[0].vel = velocidades [arriba];
            break;
        case KEY_DOWN:
            if (snake->anillo[0].pos.y + 1 != snake->anillo[1].pos.y)
                snake->anillo[0].vel = velocidades [abajo];
            break;
        case KEY_RIGHT:
            if (snake->anillo[0].pos.x + 1 != snake->anillo[1].pos.x)
                snake->anillo[0].vel = velocidades [derecha];
            break;
        case KEY_LEFT:
            if (snake->anillo[0].pos.x - 1 != snake->anillo[1].pos.x)
                snake->anillo[0].vel = velocidades [izquierda];
            break;
    }
}

void mover ( struct TSnake *snake) {
    for (int i = snake->cima-1; i > 0; i--)
        snake->anillo[i].pos = snake->anillo[i-1].pos;

    snake->anillo[0].pos.x += snake->anillo[0].vel.x;
    snake->anillo[0].pos.y += snake->anillo[0].vel.y;
}

void crecer (struct TSnake *snake) {
    if (snake->cima >= AMAX)
        return;
    else if (snake->cima != 0) {
        snake->anillo = (struct TAnillo *) realloc (snake->anillo, (snake->cima + 1) * sizeof (TAnillo));
        snake->anillo[snake->cima].pos = snake->anillo[snake->cima-1].pos;
    }
    snake->cima++;
}

void comer (struct TSnake *snake, struct TVector *fruit) {

    if (snake->anillo[INI].pos.x == fruit->x &&
            snake->anillo[INI].pos.y == fruit->y) {
        posicionar_berry (fruit);
        puntos++;
        crecer (snake);
    }
}
