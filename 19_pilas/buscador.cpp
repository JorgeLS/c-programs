#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define FILENAME "sunburn.txt"

int main(int argc, char *argv[]){

    FILE *pf;
    char *text;
    int c;
    bool end = false;

    if( argc < 2 ) {
        if ( !(pf = fopen (FILENAME, "rb")) ) {
            fprintf(stderr, "Arrr, There isn´t this file,\n");
            return EXIT_FAILURE;
        }
    }
    else {
        if ( !(pf = fopen (argv[1], "rb")) ) {
            fprintf(stderr, "Arrr, There isn´t this file\n");
            return EXIT_FAILURE;
        }
    }

    printf("Dime lo que quieres buscar: ");
    scanf(" %m[^\n]", &text);

    printf("\nLo que busco es %s\n\n", text);

    do {

        c = fgetc(pf);
        if( c == text[0] || tolower(c) == text[0] ) {
            if( strlen(text) == 1 ) {
                printf("Hay una %c en %li\n", c, ftell(pf));
                end = true;
            }
            else
                for(int i=1; i<strlen(text); i++) {
                    if( fgetc(pf) != text[i] ) break;
                    if( i == strlen(text ) - 1) {
                        printf("La palabra %s esta en %li\n", text, ftell(pf) - strlen(text));
                        end = true;
                    }
                }
        }
    }while (c != EOF && !end);

    return EXIT_SUCCESS;
}
