#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 0x100

int main(int argc, char *argv[]) {

    // Con malloc reservo en la memoria dinámica (text)
    // Sino se reserva en la direccion de memoria estática (buffer)

    char buffer[MAX];
    char * text;
    unsigned length;

    // Guarda en buffer lo que mme diga el usuario
    printf("Dime lo que quieras: ");
    scanf(" %[^\n]", buffer);

    // Guarda la longitud de buffer en length
    length = strlen(buffer);

    // Guarda en una direccion en concreto a text con una longitud de length + 1 y que sea char
    text = (char *) malloc(length + 1);
    // text = (char *) malloc( strlen(buffer) + 1 ); Puede ser así mas simplificado

    // String Copy (strcpy) copia una cadena de caracteres a otra
    // strcpy (text, buffer);
    // strcpy (text, "Hola muy buenas");
    // String n Copy (strnncpy) copia igual que strcpy pero unos determinados caracteres
    strncpy (text, buffer, MAX);

    printf("\nLa frase: %s\nEsta situada en la dirección %p\n", text, &text);

    free(text);

    return EXIT_SUCCESS;
}


