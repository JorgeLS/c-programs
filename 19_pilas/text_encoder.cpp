#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define AUM 3

int main(int argc, char *argv[]) {

    // Con malloc reservo en la memoria dinámica (text)
    char * text;
    char * p;

    // Guarda en buffer lo que me diga el usuario
    printf("Say me what do you want?: ");
    scanf(" %m[^\n]", &text);

    // Funcion con el For Largo
    for(int i=0; i<strlen(text); i++) {
        p = &text[i];
        if(*p >= 120 || (*p >= 88 && *p <=90))
            *p -= 23;
        else if(*p == 32)
            *p = 32;
        else
           *p += AUM;
    }

    printf("\nText with For long  = %s\n", text);

    // Funcion con For corto
    for ( char *pi=text; *pi!='\0'; pi++) {
        if(*pi >= 120 || (*pi >= 88 && *pi <=90))
           *pi -= 23;
        else
           *pi == 32 ? ' ' : *pi+AUM;
    }

    printf("Text with For short = %s\n", text);

    free(text);

    return EXIT_SUCCESS;
}


