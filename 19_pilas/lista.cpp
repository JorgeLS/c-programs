#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 0x100

int main(int argc, char *argv[]){

    char **lista;
    char buffer[N];
    int len, veces = 0, n;

    lista = (char **) malloc (sizeof(char *)); // Se guarda en lista las direcciones de varios char *

    do {

        lista = (char **) realloc (lista, (veces + 1) * sizeof(char *)); // Cambia el espacio reservado en memoria de lista
        printf("\nDime tu nombre: ");
        scanf(" %s", buffer);
        len = strlen (buffer);

        lista[veces] = (char *) malloc (len + 1);         // Se guarda la direccion de una palabra que tenga len + 1
        strncpy(lista[veces], buffer, len+1);                 // Por seguridad se pone en strncpy

        printf("\n"
                "1.- Otro Nombre\n"
                "2.- Salir\n"
                "Dime un numero: ");
        scanf(" %i", &n);
        veces++;
    }while(n < 2);

    lista[veces] = NULL;

    for (char **palabra = lista; *palabra != NULL; palabra++)
        printf("Nombre = %s\n", *palabra);

    for(int i=0; i<veces; i++)
        free (lista[i]);
    free (lista);

    return EXIT_SUCCESS;
}
