#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FILENAME "sunburn.txt"
#define MAX_ABC ('z' - 'a' + 1)

void put_abecedario (int M[MAX_ABC]) {

    int letra = 'a';

    for(int i=0; i<MAX_ABC; i++) {
        M[i] = letra++;
    }
}

void contar (char * text, int C[MAX_ABC], int M[MAX_ABC], int long_text) {

    for(int i=0; i<long_text; i++) {
        if(text[i] == 32) continue;

        for(int j=0; j<MAX_ABC; j++) {
            if(text[i] == M[j] || text[i] == M[j] - 32)
                C[j]++;
        }
    }
}

int main(int argc, char *argv[]) {

    FILE * pf;
    char * text;
    long end_FILE;
    int abecedario[MAX_ABC],
        contador[MAX_ABC];
    double freq = 0;

    // Pone el array contador a 0 porque no lo inicializamos sino que lo sumamos directamente a lo que haya
    bzero (contador, sizeof(contador));

    if(argc < 2) {
        if ( !(pf = fopen (FILENAME, "rb")) ) {
            fprintf(stderr, "Arrr, There isn´t this file,\n");
            return EXIT_FAILURE;
        }
    }
    else {
        if ( !(pf = fopen (argv[1], "rb")) ) {
            fprintf(stderr, "Arrr, There isn´t this file\n");
            return EXIT_FAILURE;
        }
    }

    // Cojemos el numero donde está el final
    fseek (pf, 0, SEEK_END);
    end_FILE = ftell (pf);
    rewind (pf);

    text = (char *) malloc(end_FILE);

    // Mete en text todo lo del fichero
    fread(text, sizeof(char), end_FILE, pf);

    // Pone todo el abecedario en la variable
    put_abecedario(abecedario);

    // Cuenta todas las letras que haya en el FILE
    contar(text, contador, abecedario, end_FILE);

    // Salida de Datos
    for(int i=0; i<MAX_ABC; i++) {
        freq += 100. * contador[i] / end_FILE;
        printf("\t%c.- %3.2lf %% \n", abecedario[i] - 32, 100. * contador[i] / end_FILE);
    }
    printf("\tHay un %.2lf %% que no son letras \n", 100. - freq);

    free(text);
    fclose(pf);

    return EXIT_SUCCESS;
}

/*
    const char *dedo = texto;

    while(*dedo != '\0')
      dedo++;

          ||
          ||

    for (const char *dedo; *dedo != '\0'; dedo++)
*/
