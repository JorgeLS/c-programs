#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 0x100

int main(int argc, char *argv[]) {

    // Con malloc reservo en la memoria dinámica (text)
    // Sino se reserva en la direccion de memoria estática (buffer)

    char * text;

    // Guarda en buffer lo que me diga el usuario
    printf("Dime lo que quieras: ");
    scanf(" %ms", &text);

    printf("\nLa frase: %s\nEsta situada en la dirección %p\n", text, &text);

    return EXIT_SUCCESS;
}


