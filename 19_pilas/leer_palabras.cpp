#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 0x100

int main (int argc, char *argv[]) {

    char buffer[N];
    int len, k;
    char **palabra = NULL;

    printf("Dime una palabra. Si ya no me la quieres decir escribe(salir o exit)\n");

    for(k=0; strcmp(buffer, "salir\n") && strcmp(buffer, "exit\n"); k++) {
        // Cambia en tamaño de palabra
        palabra = (char **) realloc (palabra, (k+1) * sizeof(char *));
        printf("Palabra: ");
        fgets(buffer, N-1, stdin);

        len = strlen (buffer);
        palabra[k] = (char *) realloc(palabra[k], len);
        strncpy (palabra[k], buffer, len);
        palabra[k][len-1] = '\0'; // Si pones solo len se queda el \n
    }

    free(palabra[k-1]);
    palabra[k-1] = NULL;

    for(int i=0; palabra[i]; i++)
        printf("\t%s\n", palabra[i]);

    for(int i=0; palabra[i]; i++)
        free (palabra[i]);

    free (palabra);

    return EXIT_SUCCESS;
}
