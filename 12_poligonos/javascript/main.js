var width=1300
var height=800
var XC = width / 2
var YC = height / 2
var r1, r2
var side
var clear
var alpha = 0
var R = []
var ctx = null

/*
  n: number of points
  r1: inner radius
  r2: outer radius
*/

function star(n,r1, r2){
    var p = []
    var r = [r1, r2]
    var delta = Math.PI / n
    for (i=0; i<2*n; i++)
        p.push([r[i%2] * Math.cos(i * delta), r[i%2] * Math.sin(i * delta)])
    // r[i%2] primero coje el r1 y luego r2 0-1-0-1-0-1-0-1 --> r1-r2-r1-r2-r1-r2

    return p
}

/*
  Converts world coordinates to screen ones
  p: A single point specified as an array.
*/

function s(p){
    return [XC+p[1], YC-p[0]]
}

/*
  var R = [[cos, sen],
          [-sen, cos]]

  Draw a list of points
  p: Array of points
*/

function traza(p){
    var c
    ctx.beginPath()
    c = s(p[0])
    ctx.moveTo(c[0], c[1])
    for (var i=1; i<p.length; i++){
        c = s(p[i])
        ctx.lineTo(c[0], c[1])
    }
    c = s(p[0])
    ctx.lineTo(c[0], c[1])
    ctx.stroke()
}

function borra(){
    ctx.clearRect(0, 0, width, height)
}

/*
  Multiplica una lista de puntos por una matriz
*/

function multiplica(m, l){
    var g = [] // Puntos girados
    for (var p=0; p<l.length; p++) //Para cada uno de los puntos
        g[p] = [
            l[p][0] * m[0][0] - l[p][1] * m[1][0],
            l[p][0] * m[0][1] + l[p][1] * m[1][1]
               ]
    return g
}

function reload(){
    clear = document.getElementById("clear").checked   // Mira clear y da el valor de checked
    r1 = parseInt(document.getElementById("radio1").value) // Coje del atributo values de radio 1
    r2 = parseInt(document.getElementById("radio2").value) // Coje del atributo values de radio 2
    side = parseInt(document.getElementById("side").value) // Coje del atributo values de sides
    alpha = parseInt(document.getElementById("angle").value) * Math.PI / 180  // Coje value y lo cambia a º
    R = [[Math.cos(alpha), Math.sin(alpha)],
         [-Math.sin(alpha), Math.cos(alpha)]] // Matriz de rotacion
    if (clear)
        borra();
    traza(multiplica(R, star(side, r1, r2))) //multiplica y traza
}

function main() {
    ctx = document.getElementById("lienzo").getContext("2d")
reload()
}
