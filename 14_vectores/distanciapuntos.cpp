#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "naca_23015.h"

#define PUNT 6
#define DIM 2

/*
void resta1(double p1[DIM], double p2[DIM], double r[DIM]){
    r[0] = p1[0] - p2[0];
    r[1] = p1[1] - p2[1];
}
double function1(double punto[DIM]){
    //modulo
    return sqrt( pow(punto[0], 2) + pow(punto[1], 2) );
}
*/
double resta(double p1, double p2){
    return p1 - p2;
}

double modulo(double punto1, double punto2){
    return sqrt( pow(punto1, 2) + pow(punto2, 2) );
}

double tangente(double modulo, double punto1, double punto2){
    return resta ( punto1, punto2 ) / modulo;
}

int main(){

    //La matriz perfil esta en naca_23015
    int n_puntos = sizeof(perfil)/sizeof(double)/2;
    // sizeof(perfil)/sizeof(double)/2 = 4 = sizeof(perfil)/sizeof(perfil[0]) y es /2 porque perfil[0] = 8 ya que son 2 doubles

    double distancia[n_puntos], pDis[DIM];

    for(int i=0; i<n_puntos-1; i++){
        distancia[i] = modulo(
                resta( perfil[i+1][0], perfil[i][0] ),
                resta( perfil[i+1][1], perfil[i][1] )
                );
    }

    for(int i=0; i<n_puntos-1; i++){
        printf("El modulo del punto (%.4lf, %.4lf) = %3.4lf",resta( perfil[i+1][0], perfil[i][0] ),
                                                             resta( perfil[i+1][1], perfil[i][1] ),
                                                             distancia[i]);
        printf(" y su vector tangente es (%.4lf, %.4lf)", tangente( distancia[i], perfil[i+1][0], perfil[i][0] ),
                                                          tangente( distancia[i], perfil[i+1][1], perfil[i][1] ));
        printf(" y el modulo del vector tangente es %.lf\n", modulo(
                                                tangente( distancia[i], perfil[i+1][0], perfil[i][0] ),
                                                tangente( distancia[i], perfil[i+1][1], perfil[i][1] )
                                                ));
    }

    return EXIT_SUCCESS;
}


