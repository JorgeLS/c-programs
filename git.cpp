#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){

    /* First COMMANDS FOR GIT */
    int resets, commits;

    system("git pull");
    system("git add .");
    system("git status");

    /* GIT RESETS */
    printf("Dime cuantos resets quieres?: ");
    scanf(" %i", &resets);

    char resetF[resets][80];
    char carpetaR[resets][50];

    for(int i=0; i<resets; i++){

        printf("CARPETA: ");
        scanf(" %[^\n]", carpetaR[i]);

        strcpy(resetF[i], "git reset HEAD ");
        strcat(resetF[i], carpetaR[i]);
        strcat(resetF[i], " ");

        printf("%s\n\n", resetF[i]);
    }

    for(int i=0; i<resets; i++)
        printf("\n%s\n", resetF[i]);
    for(int i=0; i<resets; i++)
        system(resetF[i]);


    system("git status");

    /* GIT COMMITS */
    printf("Dime cuantos commits quieres?: ");
    scanf(" %i", &commits);

    char commitF[commits][80];
    char carpetaC[commits][50];
    char commit[commits][50];


    for(int i=0; i<commits; i++){

        printf("CARPETA: ");
        scanf(" %s", carpetaC[i]);
        printf("COMMIT: ");
        scanf(" %[^\n]", commit[i]);

        strcpy(commitF[i], "git commit ");
        strcat(commitF[i], carpetaC[i]);
        strcat(commitF[i], " -m \"");
        strcat(commitF[i], commit[i]);
        strcat(commitF[i], "\"");

        printf("%s\n\n", commitF[i]);
    }

    for(int i=0; i<commits; i++)
        system(commitF[i]);

    /* LAST COMMANDS*/
    system("git status");
    system("git push");

    return 0;
}
