#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

typedef int temp_file_handle;


temp_file_handle write_temp_file (char* buffer, size_t length)
{
    char temp_filename[] = "/tmp/temp_file.XXXXXX";
    // Crea un inodo de temp_filename y lo guarda en fd (temp_file_handle)
    int fd = /* 1. Rellena esta linea */ mkstemp (temp_filename);

    /* Remove the especified file - Borra el fichero especifico */
    unlink (temp_filename);
    // Usamos el unlink para no escribir sobre el archivo original

    write (fd, &length, sizeof (length));

    write (fd, buffer, length);

    return fd;
}

char* read_temp_file (temp_file_handle temp_file, size_t* length)
{
    char* buffer;
    int fd = temp_file;

    lseek (fd, 0, SEEK_SET);

    read (fd, length, sizeof(*length));

    buffer = (char*) malloc (*length);
    read (fd, buffer, *length);

    close (fd);
    return buffer;
}

int main () {


    temp_file_handle manejador;
    char mensaje[] = "Hello World";
    size_t longitudMensaje = strlen(mensaje);
    char* mensajeFichero;

    manejador = write_temp_file (mensaje, longitudMensaje);

    mensajeFichero = read_temp_file (manejador, &longitudMensaje);

    printf ("El mensaje es: %s\n", mensajeFichero);

    free (mensajeFichero);
    close (manejador);

    return EXIT_SUCCESS;
}
