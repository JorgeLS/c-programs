#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>

int spawn (char* program, char* arg_list[]) {

    pid_t child_id;

    child_id = fork ();

    // Padre
    if (child_id != 0) {
        printf ("Se ha llamado al padre\n");
        return child_id;
    }
    // Hijo
    else {

        execvp (program, arg_list);

        fprintf (stderr, "No se ha podido ejecutar el programa. %s\n", program);

        abort();
    }

    return child_id;
}

int main (int argc, char* argv[]) {

    pid_t child_id;
    int child_status;
    char* arg_list[3];

    arg_list[0] = (char *) "./contador";
    arg_list[1] = NULL;

    child_id = spawn (arg_list[0], arg_list);

    for (int i = 0; i < 3; i++) {
        sleep (1);
        kill (child_id, SIGUSR1);

        printf ("Señal enviada al hijo\n");
    }

    kill (child_id, SIGINT);
    
    wait (&child_status);

    if (WIFEXITED(child_status))
        printf ("El proceso hijo termino bien: %i\n", WEXITSTATUS(child_status));
    else
        printf ("El proceso hijo no termino\n");

    return 0;
}
