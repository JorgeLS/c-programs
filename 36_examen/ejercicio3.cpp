#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>
#include <semaphore.h>

#define ladrillosMAX 600

sem_t job_queue_count;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

struct TLadrillos {
	int fabricados;
	int almacen;
	int recogidos;
};

void* almacenar (void* parameters) {

	// Para solo ver parametros pongo esto
	// struct TLadrillos lad = *(struct TLadrillos *) parameters;
	struct TLadrillos *lad = (struct TLadrillos *) parameters;

	for (int i = 0; i < ladrillosMAX; i++) {
		
		pthread_mutex_lock (&mutex);
		
		sem_post (&job_queue_count);

		lad->fabricados++;
		lad->almacen++;

		//printf ("Se ha fabricado un ladrillo LF-%i\n", lad->fabricados);
		
		pthread_mutex_unlock (&mutex);

		usleep(700000);
	}

	return NULL; 

}

void* recoger (void* parameters) {

	struct TLadrillos *lad = (struct TLadrillos *) parameters;
	int recogidos = 0;

	while (lad->recogidos != ladrillosMAX) {


		if (lad->almacen > 0)
		{
			
			sem_wait (&job_queue_count);
			
			pthread_mutex_lock (&mutex);

			recogidos = rand() % 2 + 1;

			if (lad->almacen - recogidos >= 0)
			{
				lad->almacen -= recogidos;
				lad->recogidos += recogidos;
			}
			else {
				lad->almacen--;
				lad->recogidos++;
			}
			
			pthread_mutex_unlock (&mutex);
		}

		usleep ( 1000000 * (rand() % 5 + 1) );
	}

	return NULL;
}

void* mandar (void* parameters) {

	struct TLadrillos *lad = (struct TLadrillos *) parameters;

	pthread_t id_manolito;
	pthread_t id_benito;
	
	// Creamos los hilos de los obreros
	pthread_create (&id_manolito, NULL, &recoger, lad);
	pthread_create (&id_benito, NULL, &recoger, lad);

	pthread_join (id_manolito, NULL);
	pthread_join (id_benito, NULL);

	printf ("Los obreros han terminado\n");

	return NULL;
}

void* mostrar (void* parameters) {

	struct TLadrillos *lad = (struct TLadrillos *) parameters;

	while (lad->recogidos < 600) {

		pthread_mutex_lock (&mutex);

		fprintf (stderr, "\rLadrillos: Fabricados: %i  Almacen: %i  Recogidos: %i", lad->fabricados, lad->almacen, lad->recogidos);
		fflush (stderr);

		pthread_mutex_unlock (&mutex);

		usleep(500000);
	}


	return NULL;
} 

int main () {

	srand (time(NULL));

	struct TLadrillos ladrillos;

	ladrillos.fabricados = 0;
	ladrillos.almacen = 0;
	ladrillos.recogidos = 0;

	sem_init (&job_queue_count, 0, ladrillos.almacen);
	
	pthread_t id_fabrica;
	pthread_t id_capataz;
	pthread_t id_mostrar;

	// Creamos los hilos
	pthread_create (&id_fabrica, NULL, &almacenar, &ladrillos);
	pthread_create (&id_capataz, NULL, &mandar, &ladrillos);
	pthread_create (&id_mostrar, NULL, &mostrar, &ladrillos);

	// Primero espera a que termine la fabrica
	pthread_join (id_fabrica, NULL);
	// Después espera a que termine el capataz
	pthread_join (id_capataz, NULL);

	printf ("\nLA = %i && LR = %i\n", ladrillos.almacen, ladrillos.recogidos);

	return 0;
}