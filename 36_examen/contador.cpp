#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

//int sigusr1_count = 0;
sig_atomic_t sigusr1_count = 0;

void counter (int signal) {
    
    if (signal == SIGUSR1)
        sigusr1_count++;
    else
        printf ("The received signal is not SIGUSR1\n");
}

int main () {

    /*
     * El hijo va ha ser el que tiene la espera para recibir
     * la señal (SIGUSR1) con la funcion de sigaction y cuando
     * la reciba va ha ser ejecutado counter.
     *
     * The child process will be waiting to receive the signal (SIGUSR1)
     * with the sigaction function and when it receives the signal
     * it will be execute counter.
    */

    // Creamos un struct sigaction en el hijo para recibir los mensajes
    // Create a struct sigaction in the child process to receive the message
    struct sigaction sa;

    // Guardamos un espacio en la memoria
    // Store a space in memory
    memset (&sa, 0, sizeof (sa));

    // Decimos que funcion va ha ser llamada
    // Set the call function
    sa.sa_handler = &counter;

    // Especificamos la señal ha recibir
    // Specify the receive signal
    sigaction (SIGUSR1, &sa, NULL);

    for (int i = 0; sigusr1_count < 3; i++) {
        printf ("Count = %i y SIGUSR1 = %i\n", i, sigusr1_count);
        usleep (60000);
    }

    return 0;
}
