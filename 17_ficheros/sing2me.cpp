#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]){

    const char * cancion;
    int c;
    FILE *pf;

    if( argc < 2)
        return EXIT_FAILURE;
    cancion = argv[1];

    if( !(pf = fopen (cancion, "r")) )
        return EXIT_FAILURE;

    while ( (c = fgetc (pf)) != EOF)
        printf("%c", c);

    fclose (pf);

    return EXIT_SUCCESS;
}


