#include <stdio.h>
#include <stdlib.h>

#define F "frases.txt"
#define VECES 10
#define DIM 100
int main(int argc, char *argv[]){

    FILE *fichero;

    char p[DIM];

    if( !(fichero = fopen(F, "w")) )
        return EXIT_FAILURE;

    //Forma facil de hacer
    /*
    printf("Dime 10 buenas frases:\n");
    for(int i=0; i<VECES; i++){
        printf("Frase Nº%i: ", i+1);
        scanf(" %[^\n]", p);
        fprintf(fichero, "%s\n", p);
    }
    OR
    */

    for(int i=0; i<VECES; i++){
        printf("Frase Nº%i: ", i+1);
        fgets (p, DIM, stdin);
        fprintf (fichero,"%s", p);
    }
    fclose (fichero);

    return EXIT_SUCCESS;
}


