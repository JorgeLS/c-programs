#include <stdio.h>
#include <stdlib.h>

#define NOMBRE "cancion.txt"

const char * song = "\n\
                The hard times are reigning \n \
                The world is in pain \n \
                The kings and the captains they all conquer in vain \n \
                I stand in the background \n \
                Just watching the scene \n \
                I'm doing my best to keep my hands washed and clean \n \
                The questions are rising \n \
                Are drifting like snow \n \
                If you never get no answer you'll never know \n \
                The more I did wonder \n \
                The less I did care \n \
                When one day suddenly the answer was there \n \
                I heard the man in the sky \n \
                He was talking right to me \n \
                He said it's very simple \n \
                This is how it's gonna be \n \
                Your life is your own business \n \
                You can even cheat and lie \n \
                But you never can count away the man in the sky \n \
                I could hear him talking \n \
                Tho' he didn't speak \n \
                I just understood that I am strong when I'm weak \n \
                'Cause man was created \n \
                To work all alone \n \
                The answer is written in the soul of your own \n \
                I heard the man in the sky \n \
                He was talking right to me \n \
                He said it's very simple \n \
                This is how it's gonna be \n \
                Your life is your own business \n \
                You can even cheat and lie \n \
                But you never can count away the man in the sky \n \
                So live your life as you please \n \
                And let your mind command \n \
                'Cause when it really comes down to it you are just a man \n \
                The only thing you know for sure \n \
                Is that you're gonna die \n \
                So you can never count away the man in the sky \n \
                The only thing you know for sure \n \
                Is that you're gonna die \n \
                So you can never count away the man in the sky \n\n"
                ;

void print_usage() {
    printf("Wsto no se usa así\n");
}

void informo (const char *mssg) {
    print_usage();
    fprintf(stderr, "%s\n", mssg);
    exit(1);
}

int main(int argc, char *argv[]) {

    FILE *fichero;

    if( !(fichero = fopen ( NOMBRE, "w" )) )
        informo ("No se ha podido abrir el fichero.");
    fprintf(fichero, "%s", song);
    getchar ();
    fclose (fichero);

    return EXIT_SUCCESS;
}
