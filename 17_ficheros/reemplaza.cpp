#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define N 0x100
#define BAD_OPTION 1
#define BAD_FILENAME 2

const char *  progname;

void print_usage (FILE *outstream, int exit_code) {
    fprintf ( outstream, "\
Usage:      %s [options] \n\
\n\
            - Replace all the ocurrences of seek characters (s) with \n\
            replace characters (r) within file (f). \n\
\n\
Options:\n\
            -f <filename>   \t Defaults to stdin. \n\
            -r <character>  \t Defaults to '!'. \n\
            -s <character>  \t Defaults to 'i'. \n\
            -h              \t Prints help on screen. \n\
\n\
Example: ./reemplaza -rd -sb -fa.txt \n\n\
", progname);
    exit (exit_code);
}

int main(int argc, char *argv[]) {

    int o;
    progname = argv[0];
    char r = '!'; /* r: char to replace with */
    char s = 'i'; /* s: char to look for */
    char f = '-'; /* f: file name. '-' equals to stdin */
    char filename[N];

    while ( (o = getopt (argc, argv, "hf:r:s:")) != -1) { // : means optional argument
        switch (o){
            case 'h':
                print_usage (stdout, 0);
                break;
            case 'f':
                // ./reeplaza -f a.txt // optarg points to a in a.txt
                strncpy (filename, optarg, N);
                f = '\0';
                break;
            case 'r':
                r = *optarg; //optarg = Optional Argument
                break;
            case 's':
                s = *optarg;
                break;
            case '?':
                if (optopt != 'f' && optopt != 'r' && optopt != 's')
                    fprintf (stderr, "Invalid Option.\n");
                else
                    fprintf (stderr, "Option argument missing.\n");
                print_usage (stderr, BAD_OPTION);
                break;
            default:
                abort ();
        }
    }

    FILE *pf = stdin;
    int c;

    if(!f)
        if( !(pf = fopen (filename, "r")) ){
            fprintf (stderr, "Couldn't find your %s\n", filename);
            print_usage(stderr, BAD_FILENAME);
        }

    while ( (c = getc (pf)) != EOF ){ //fgetc == getc Catch a character from stream
        //usleep(5000);
        printf("%c", c == s ? r : c );
    }

    fclose(pf);

    return EXIT_SUCCESS;
}


