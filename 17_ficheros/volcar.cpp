#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100
#define N 10

void say_me (char nombre[N][MAX]) {

    printf("Say me 10 Names\n");
    for(int i=0; i<N; i++){
        printf ("Name Nº%i: ", i+1);
        scanf (" %[^\n]", nombre[i]);
        //Or can put fgets (&nombre[i], MAX, stdin);
    }
}

void volcar_a_fichero ( char M[N][MAX], FILE *file) {

    for(int i=0; i<N; i++)
        fprintf (file, "%s\n", M[i]);
}

int main(int argc, char *argv[]) {

    /* Put in the file in that will have nombre */
    char nombre[N][MAX];
    FILE *pf = NULL;

    say_me (nombre);

    if( !( pf = fopen (argv[1], "w+") ) ) {
        fprintf(stderr, "Shit, You must put ./volcar (nameFile)\n");
        return EXIT_FAILURE;
    }

    volcar_a_fichero (nombre, pf);
    fclose (pf);

    return EXIT_SUCCESS;
}
