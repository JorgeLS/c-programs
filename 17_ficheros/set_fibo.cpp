#include <stdio.h>
#include <stdlib.h>

#define DIM 15
#define FILENAME "fibonnacci.dat"

// Sucesión de fibonnacci
int rellenar ( int M[DIM], int i ) {
    if(i == 1){
        M[0] == 1;
        return M[1] = 1;
    }
    return M[i] = rellenar (M, i-1) + M[i-2];
}

int main(int argc, char *argv[]){

    int fibonnacci[DIM];

    rellenar ( fibonnacci, DIM-1 );

    FILE *pf;
    int num_elementos = sizeof(fibonnacci)/sizeof(int);

    if( !(pf = fopen (FILENAME, "wb")) ) {
        fprintf(stderr, "Douch!!!\n");
        return EXIT_FAILURE;
    }

    fwrite (fibonnacci, sizeof(int), num_elementos, pf);

    fclose(pf);

    return EXIT_SUCCESS;
}


