#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100
#define N 10

void volcar_a_matriz (char M[N][MAX], FILE *file) {

    for(int i=0; i<N; i++)
        fgets (M[i], MAX, file);
}

int main(int argc, char *argv[]) {

    /* Put in matriz all in a file */
    FILE *fp = NULL;
    char matriz[N][MAX];

    if( !( fp = fopen (argv[1], "r") ) ) {
        fprintf(stderr, "Shit, You must put ./volcar (nameFile)\n");
        return EXIT_FAILURE;
    }

    volcar_a_matriz (matriz, fp);

    for(int i=0; i<N; i++)
        printf("%s\n", matriz[i]);

    fclose (fp);

    return EXIT_SUCCESS;
}
