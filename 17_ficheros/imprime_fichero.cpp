#include <stdio.h>
#include <stdlib.h>

#define NOMBRE "frases.txt"

int main(int argc, char *argv[]){

    FILE *fichero;
    int p;

    if( !(fichero = fopen(NOMBRE, "r")) )
        return EXIT_FAILURE;

    do{
        p = fgetc(fichero);
        if(p != 'i')
            printf("%c", p);
        else
           printf("!");
    }while(p != EOF);

    printf("\n");
    fclose (fichero);
    return EXIT_SUCCESS;
}


