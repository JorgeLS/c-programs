#include <stdio.h>
#include <stdlib.h>

#define DIM 15
#define FILENAME "fibonnacci.dat"

int main(int argc, char *argv[]){

    int fibonnacci[DIM];
    FILE *pf;
    int num_elementos = sizeof(fibonnacci)/sizeof(int);

    if( !(pf = fopen (FILENAME, "rb")) ) {
        fprintf(stderr, "Douch!!!\n");
        return EXIT_FAILURE;
    }

    fread (fibonnacci, sizeof(int), num_elementos, pf);

    fclose(pf);

    for(int i=0; i<DIM; i++)
        printf("\t%i\n", fibonnacci[i]);

    return EXIT_SUCCESS;
}


