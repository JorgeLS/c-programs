#include <stdio.h>
#include <stdlib.h>

#define FILENAME "matriz.txt"

int main(int argc, char *argv[]){

    FILE *pf;
    long int inicio, fin, distancia;

    pf = fopen(FILENAME, "r");

    //Esta en el inicio
    inicio = ftell(pf);
    //Mueve el cursor hasta el final
    fseek (pf, 0, SEEK_END);
    //Esta en el final
    fin = ftell(pf);
    //Final - Inicio
    distancia = fin - inicio;

    printf("Inicio: %li\n"
           "Final: %li\n"
           "Distancia: %li\n",
           inicio, fin, distancia);

    fclose(pf);

    return EXIT_SUCCESS;
}


