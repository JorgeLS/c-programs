#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define FILENAME "spanish_words.txt"
#define N 0x100
#define NUM 48247
#define LMUERTE 8

char palabra_introducida[N];
int l = 0;

void dibuja() {

    printf("\n");

    system("clear");
    system("toilet -fpagga -Fcrop:border:metal '   AHORCADO   '");
    __fpurge(stdin);

    printf("\n\n\n\t");
}

void get_word (char *word) {

    FILE *pf;
    char buffer[N];
    int len, i;
    char **palabra = NULL;

    if ( !(pf = fopen(FILENAME, "r")) ) {
        fprintf (stderr, "No... no veo nada.\n");
        exit (EXIT_FAILURE);
    }

    // palabra = (char **) malloc (sizeof(char *));
    palabra = (char **) calloc(NUM, sizeof(char*));

    for(i=0; i<NUM; i++) { //48250
        // do{
        // palabra = (char **) realloc (palabra, (i+1) * sizeof(char *));
        fgets(buffer, N-1, pf);

        len = strlen (buffer);
        palabra[i] = (char *) realloc(palabra[i], len);
        strncpy (palabra[i], buffer, len);
        palabra[i][len-1] = '\0'; // Si pones solo len se queda el \n
    }

    free(palabra[i-1]);
    palabra[i-1] = NULL;

    strcpy(word, palabra[rand() % NUM]);

    // Cerrado de Datos dinámicos
    for(int j=0; palabra[j]; j++)
        free (palabra[j]);

    fclose(pf);
    free (palabra);
}

char get_letter () {

    char letter = ' ';
    bool comp = false;

    printf("Dime una letra: ");
    scanf(" %c", &letter);

    // Comprueba que esa letra no esté puesta y sea una letra minúscula
    for(int i=0; i<N; i++)
        if( letter == palabra_introducida[i] )
            comp = true;

    if(!comp) {
        if(letter > 96 && letter < 123)
            return letter;
        else{
            printf("\nSolo puedes introducir letras minúsculas, %c\n", letter);
            return '-';
        }
    }
    else {
        printf("Esa letra ya la has puesto\n");
        return '-';
    }

    return letter;
}

bool comprobacion (char *word1, char *word2, char letter) {

    bool check = false;

    if (letter == '-') {
        comprobacion (word1, word2, get_letter());
        check = true;
    }
    for(int i=0; i<strlen(word1)+1; i++)
        if(word1[i] == letter) {
            word2[i] = letter;
            check = true;
        }

    if( !check )
        palabra_introducida[l++] = letter;

    return check;
}

int main (int argc, char *argv[]) {

    srand(time(NULL));

    // char wordA[N] = "hola";
    char wordA[N];
    char wordR[N];
    char letra;
    bool end = false;
    int turno = 0;

    get_word(wordA);

    strcpy(wordR, wordA);

    for(int i=0; i<strlen(wordR); i++)
        wordR[i] = '_';

    for(int i=0; i<N; i++)
        palabra_introducida[i] = ' ';

    do {

        dibuja();

        for(int i=0; i<strlen(wordR); i++)
            printf("%c", wordR[i]);

        printf("\n\n\nLetras ya puestas: ");
        for(int i=0; i<N; i++) {
            if( palabra_introducida[i] == ' ' ) break;
            printf("%c ", palabra_introducida[i]);
        }
        printf("\n\n");

        if( !comprobacion (wordA, wordR, get_letter()) ) {
            turno++;
        }

    }while(strcmp(wordR, wordA) != 0 && turno != LMUERTE);

    dibuja();
    printf("%s  ", wordR);

    if (strcmp(wordR, wordA) == 0)
        printf("\n\n    HAS ACERTADO LA PALABRA\n\n");
    else
        printf("\n\n    HAS FALLADO HAS MUERTO\n\n");

    return EXIT_SUCCESS;
}
