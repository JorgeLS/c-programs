#!/bin/bash
# Put ^ this to init a bash program

# Function
menu() {
    echo
    read -p "Write two numbers: " num1 num2
    num3=$1
}

# Clear the console
clear

# Show the count of the arguments and the first argument
echo "Argumentos: Count($#) $1"

# Call the function and add a parameter
menu 10;

# Show this variables
echo "Los tres numeros son: $num1 - $num2 - $num3"

# Simple operation
echo "+ = $(($num1 + $num2))"
echo "- = $(($num1 - $num2))"
txt="Hello"
txt+=" World!!" # Is equal than txt="$txt World!!"
echo "Sum of Strings -$txt-"

echo
# If-Then-Else
if [ $num1 -eq 2 ] # Can use == and =
  then
      echo "Hello!!"
  elif [ ! $num3 == 10 ]
  then
      echo "The program changed"
  elif [ $num1 -le $num2 ] # -le is equal to <=
  then
      echo "Hello number 2!!"
  else
      echo "Douch"
fi

echo
# Loop with while
i=0

while [ TRUE ]; do
    echo $i
    i=$((i + 1)) # Can not put i+=1 only in Strings
    # Sleep function is in seconds
    sleep 0.2

    if [ ! $i -le 4 ] # -le (less equal) -lt (less than)
      then break
    fi
done

echo
# For
for count in {0..10..3}; do # $count = 0 3 6 9
    if [ ! $(($count % 2)) = 1 ]
      then echo "Second Count: $count"
      else continue
    fi
done

echo
# Switch
case "$1" in
  Hello)
      echo "Hello too!!"
      ;;
  Bye)
      echo "Bye"
      ;;
  *)
      echo "Douch"
      ;;
esac

echo
# File
if [ -a "add.sh" ]
  then echo "El archivo add.sh existe"
  else echo "El archivo add.sh no existe"
fi

# -a TRUE if exists the file
# -d TRUE if is a directory and exists
# -f TRUE if is regular file and exists
# -r TRUE if is readable file and exists
# -w TRUE if is writable file and exists
# -x TRUE if is executable file and exists
