#!/bin/bash

PATH=/sbin:/usr/sbin:/bin:/usr/bin
NAME="servicio"
DAEMON="$NAME"
DAEMON_ARGS=""
PIDFILE=$NAME.pid
PID=$BASHPID
SCRIPTNAME=$NAME

# Check that the file exists and executable
[ -x "$DAEMON" ] || exit 0

# For VERBOSE
. /lib/init/vars.sh
# For the log menssage
. /lib/lsb/init-functions return 0

do_start () {

    # [ -f "$PIDFILE" ] && return 1
    call="$(pwd)/$DAEMON"
    setsid $call
    # setsid $call > /dev/null || return 2
}

do_stop() {

    [ -f "$PIDFILE" ] || return 1
    kill $(cat "$PIDFILE") || return 1

    pkill "$NAME" &> /dev/null
}

do_reload() {

    kill -HUP $(cat "$PIDFILE")
    return 0
}

echo "Proccess ID: $PID"

if [ ! $# == 0 ]
  then
      case "$1" in
          start)
              echo "$PID - Starting $NAME..."
              [ "$VERBOSE" != no ] && log_daemon_msg "Starting $NAME"
              do_start
              case "$?" in
                  0|1) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
                    2) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
              esac
              echo "Start ha devuelto $?"
              ;;
          stop)
              echo "$PID - Stopping $NAME..."
              [ "$VERBOSE" != no ] && log_daemon_msg "Stopping $NAME"
              do_stop
              case "$?" in
                  0|1) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
                    2) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
              esac
              echo "Stop ha devuelto $?"
              ;;
          status)
              # status_of_proc "$DAEMON" "$NAME" && exit 0 || exit $?
              ;;
          reload|force-reload)
              do_reload
              ;;
          restart)
              echo "Stop ha devuelto $?"
              log_daemon_msg "Restarting $NAME"
              do_stop
              case "$?" in
                  0|1)
                      do_start
                      case "?" in
                          0) log_end_msg 0 ;;
                          1) log_end_msg 1 ;;
                          *) log_end_msg 1 ;;
                      esac
                      ;;
                  *)
                      log_end_msg 1
                      ;;
              esac
              ;;
          *)
              echo "Usage: . daemonCall.sh {start|stop|status|restart|reload|force-reload}" >&2
              # exit 3
              ;;
      esac
  else
      echo "You must to put arguments"
fi
