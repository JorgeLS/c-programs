#!/bin/bash

# Create the file to call
call=". "
call+=$1

# Export this varibles that can use in the other file
export num1=$2
export num2=$3

if [ ! $# -lt 3 ]
  then
      # Call the function and put the arguments
      $call $num1 $num2

      # Get the ExitCode
      case $? in
          -1)
              echo "Error."
          ;;
          *)
              echo "The sum = $?"
          ;;
      esac
  else
      echo "Error put all the arguments."
      echo "Usage: add.sh {num1} {num2} "
fi
