#include <stdio.h>
#include <stdlib.h>

int main()
{
    system("clear");    

    unsigned primo[]   = {2, 3, 5, 7, 11, 13, 17, 19, 23};
    unsigned elementos = (unsigned) sizeof(primo) / sizeof(int);
    unsigned *peeping  = primo;  //La direccion de primo
    char *tom = (char *) primo;  
    unsigned **police = &peeping;   //La direccion de la direccion de primo
    //unsigned *plc[9] = primo 

    /* Esto es lo mismo que arriba
       primo[0] = 2;
       primo[1] = 3;
       primo[2] = 5;
       .
       .
       .         */

    printf( " PRIMO:\n"
            " ======\n"
            " Localización (%p)\n"
            " Elementos: %u [%u..%u]\n"
            " Tamaño: %lu bytes.\n\n", 
            primo, elementos, primo[0], primo [elementos-1], sizeof(primo));

    for(int i=0; i<elementos; i++){
        printf(" %i: %u\n", i, peeping[i]);
    }
    printf("\n");
    for(int i=0; i<elementos; i++){
        printf(" %i: %u\n", i, *(peeping + i));
    }
    printf(" Tamaño: %lu bytes.\n\n", sizeof(peeping));

    /* Memory Dump - Volcado de Memoria */
    for(int i=0; i<sizeof(primo); i++)
        printf("%02X", *(tom + i));

    printf("\n\n");

    printf("Police contiene %p\n", police);
    printf("Peeping contiene %p\n", *police);
    printf("Primo[0] contiene %u\n", **police);

    return EXIT_SUCCESS; 
}	

