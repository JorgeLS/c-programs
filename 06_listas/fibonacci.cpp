#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define N 20

int main()
{
    int emento[N];
      
    /* Condiciones de contorno */
    emento[1] = emento[0] = 1;
    
    /*Cáculos*/ 
    for(int i=2; i<N; i++)
        emento[i] = emento[i-2] + emento[i-1];

    /*Salida de datos*/
    for(int i=2; i<N; i++)
        printf("%i + %i = %i\n", emento[i-2], emento[i-1], emento[i]); 
          
    printf("\n");

    for(int i=2; i<N; i++)
        printf("%i / %i = %lf\n", emento[i], emento[i-1], (double) emento[i]/emento[i-1]);

    return EXIT_SUCCESS; 

}	


