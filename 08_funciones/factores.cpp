#include <stdio.h>
#include <stdlib.h>

#define N 7

int main()
{
    unsigned primo[ ] = {2, 3, 5, 7, 11};
    unsigned factor1[N];
    unsigned factor2[N];
    int a = 0;
    int b = 0;
    int num1 = 12, num2 = 72;

    /*Calcula los factores*/

    for(int i = 4; i>=0; i--){
        while(num1 %primo[i] == 0){
            factor1[a++] = primo[i];
            num1 /= primo[i];
        }
        while(num2 %primo[i] == 0){
            factor2[b++] = primo[i];
            num2 /= primo[i];
        }
    }

    for(int i = 0; i<a; i++)
        printf("%i y %i \t", factor1[i], i);
    printf("\n");
    for(int i = 0; i<b; i++)
        printf("%i y %i \t", factor2[i], i);
    printf("\n");

    /*Busca y quita los factores iguales*/

    for(int c = 0; c<b; c++){
        for(int i = 5; i>=0; i--){
            if(factor1[i-c] == factor2[i]){
                factor2[i] = 1;
                factor1[i-c] = 1;
            }
        }
        for(int i = 5; i>=0; i--){
            if(factor2[i-c] == factor1[i]){
                factor1[i] = 1;
                factor2[i-c] = 1;
            }
        }
    }

    printf("Denominador: ");
    for(int i = 0; i<a; i++)
        if(factor1[i] != 1)
            printf("%i\t", factor1[i]);
    printf("\n");
    printf("Nominador: ");
    for(int i = 0; i<b; i++)
        if(factor2[i] != 1)
            printf("%i\t", factor2[i]);
    printf("\n");

    return EXIT_SUCCESS;
}


