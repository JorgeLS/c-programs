#include <stdio.h>
#include <stdlib.h>

int encuentra(int buscado, int A[], int n_elem){

    int pos = -1;

    for(int i = 0; i< n_elem; i++){
        if(A[i] == buscado)
            pos = i;
    }
    return pos;

}

int main(){

    int A[] = {5, 20, 3, 9, 2, 13, 17, 10};
    int buscado = 13;

    printf("Dime un número, haber si esta en mi lista: ");
    scanf(" %i", &buscado);

    int pos = encuentra(buscado, A, sizeof(A)/sizeof(int));

    /*Busca el 13*/

    if(pos > 0)
        printf("Si y el número está en la posición %i de mi Array\n", pos);
    else
        printf("Ese número no se encuentra en el Array\n");

    return EXIT_SUCCESS;
}

/*
 * Cinco partes de las funciones:
 * 1- Como va a ser la funcion = void, int, double, boolean, char...
 * 2- Como se llama = comprueba, suma...
 * 3- Parametros que meteremos = int op1, double op2, char double[DIM][DIM], bool end...
 * 4- Lo que hace la funcion =
 *    int resultado;
 *    resutado = op1 + op2;
 *    lo que se que haga...
 * 5- Lo que devuelve return resultado, op1 + op2 ...
 * */
