#include <stdio.h>
#include <stdlib.h>

#define Rojo 1
#define Amar 2
#define Azul 4
int main()
{
      //Con if   
      bool r = false, y = false, b = false;
      char respuesta;

      printf("Ves rojo? (y/n): ");
      scanf(" %c", &respuesta);
        if (respuesta == 'y')
        r = true;
                        
      printf("Ves amarillo? (y/n): ");
      scanf(" %c", &respuesta);
        if (respuesta == 'y')
        y = true;
              
      printf("Ves azul? (y/n): ");
      scanf(" %c", &respuesta);
        if (respuesta == 'y')
        b = true;      
      
    if (r)
      if (y) 
        if (b) 
          printf("Negro.\n"); 
          else 
            printf("Naranja.\n");
      else if (b) 
        printf("Maguenta.\n");
      else 
        printf("Rojo.\n");
    else if (y)
      if (b) 
        printf("Verde.\n");
      else 
        printf("Amarillo.\n");
    else if (b)
      printf("Azul.\n");
      else
      printf("Blanco.\n");
    
    //Con switch
    int color = 0;
    
      printf("Ves rojo? (y/n): ");
      scanf(" %c", &respuesta);
        if (respuesta == 'y')
        color += 1;
        
      printf("Ves amarillo? (y/n): ");
      scanf(" %c", &respuesta);
        if (respuesta == 'y')
        color += 2;
                      
      printf("Ves azul? (y/n): ");
      scanf(" %c", &respuesta);
        if (respuesta == 'y')
        color += 4;      
     
      switch(color){
          case 0:
            printf("Blanco.\n");
            break;
          case 1:
            printf("Rojo.\n");
            break;    
          case 2:
            printf("Amarillo.\n");
            break;
          case 3:
            printf("Naranja.\n");
            break;   
          case 4:
            printf("Azul.\n");
            break;
          case 5:
            printf("Magenta.\n");
            break;    
          case 6:
            printf("Verde.\n");
            break;
          case 7:
            printf("Negro.\n");
            break;    
      }


      return EXIT_SUCCESS; 
}


