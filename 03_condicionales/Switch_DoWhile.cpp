#include <stdio.h>
#include <stdlib.h>

int main()
{ 
    int a=0,b,c;
    
    printf("Switch\n");
    scanf("%d", &a);
     
    switch(a){
        case 1: puts("Pepe");
                  break;
        case 2: puts("Pepito");
                  break;
        case 3: puts("Pepazo");
                  break;           
        default: puts("Pocoyo");
                 break;
                 }
    
    puts("\nDo while\n");

    b = 0;
    do{
    puts("Hola muy buenas!!");
    b++;
    }
    while(b < 5);   

    printf("\n");
    return EXIT_SUCCESS; 
}	


