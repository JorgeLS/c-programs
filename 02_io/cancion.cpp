#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>  //This library is for put the usleep

#define N  4 
#define T1 1000000  
#define T2 750000 
#define T3 500000 
#define T4 250000 

#define VECES 12
#define BTU 100000 /*Basic Time Unit */

void pitido(){
fprintf(stderr, "\a");
}

int main()
{
	//60
	int a=0;

        while (a<N){
		pitido();
        	usleep(T3);
                pitido();
		usleep(T4);
		pitido();
		usleep(T3);
                pitido();
		usleep(T3);
  		pitido();
		usleep(T3);
		pitido();
		usleep(T2);
		pitido();
		usleep(T1);
		
		pitido();
		usleep(T3);
		pitido();
		usleep(T4);
		pitido();
		usleep(T3);
  		pitido();
		usleep(T3);
		pitido();
		usleep(T1);
		pitido();
		usleep(T1);
		a++;									
	}

	a=0;
	while(a<N){
		pitido();
		usleep(T2);
		pitido();
		usleep(T3);
		pitido();
		usleep(T3);
  		pitido();
		usleep(T4);
		pitido();
		usleep(T2);
		pitido();
		usleep(T1);
		pitido();
		usleep(T1);
		a++;									
	
	}
 	
	int duracion[VECES] = {0, 2, 4, 4, 8, 5, 3, 2, 4, 1, 1};	
	for (int i=0; i<VECES; i++) {
		usleep(duracion[i] * BTU);
		fputc('\a', stderr);
		
	}
	
	printf("\n");
	return EXIT_SUCCESS; 
}	


