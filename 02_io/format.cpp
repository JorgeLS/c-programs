#include <stdio.h>
#include <stdlib.h>

int main()
{
	printf("%c", 97);
	printf("%c", 0x61);
	printf("%c", 'a');

	printf("%i", 97);
	/*Lo que hace %i cuando comvierte 457:
	 4 => '4'
	 '4' - '0' = 4
	 '5'
	 4*10=40
	 '5' - '0' = 5
 	 40 + 5 = 45
	 '7'
	 45*10 = 450
	 '7' - '0' = 7
	 450 + 7 = 457
	 457	 
	  */
	printf("\n");	
	
	
	printf("%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n",1,2,3,4,5,6,7,8,9,10,11,12,13);
	printf("%lf\n", 3.2);	
	printf("%.2lf\n", 3.2);	
	printf("%4.2lf\n", 3.2);	
	printf("%6.2lf\n", 3.2);	
	
	printf("\thola\n");
	
	int numero;
	printf("Num:");
	scanf("%i", &numero);
	printf("Numero => [%p]: %i\n", &numero, numero);
	printf("Linea %i\n", 39);
	
	int dia, mes, annio;
	printf("Nacimiento dd/mm/aaaa: ");
	scanf("%d/%d/%d", &dia, &mes, &annio);
	
	char pip[32], pep[32];
        printf("Conjuntos de Selección: ");        
	scanf("%[0-9abcdefA-F]", pip); //Conjunto de selección 
	printf("%s", pip);
        scanf("%[^0-9abcdefA-F]", pep); //Conjunto de selección inverso
        printf("%s", pep);

	printf("\n");
        return EXIT_SUCCESS; 
}	


