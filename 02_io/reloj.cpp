#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define N 360000
#define DELAY 100

int main(){

    int h=0, m=0, s=0;

    for(int i=0; i<N; i++){
        fprintf(stderr, "%2i:%2i:%2i\r",h,m,s);
        usleep(DELAY);
        s++;

        if(s == 60){
            m++;
            s = 0;
            if(m == 60){
                h++;
                m = 0;
                if(h == 24){
                    h = 0;
                }
            }
        }
    }

    printf("\n");

    return 0;
}
