#include <stdio.h>
#include <stdlib.h>

int main(){
	char nombre[32],
             apellido1[32],
	     apellido2[32];	

	printf("Dime tu nombre y apellidos: ");//stdout
	scanf(" %s %s %s", nombre, apellido1, apellido2);//stdin
	printf("Hola %s %s %s.\n", nombre, apellido1, apellido2);

        //Para Hacerlo mejor

        char nombrecompleto[32];
        
        printf("Dime tu nombre completo: ");
        scanf(" %[^\n]", nombrecompleto);
        printf("Hola %s\n", nombrecompleto);        
        
        return EXIT_SUCCESS; 
}	


