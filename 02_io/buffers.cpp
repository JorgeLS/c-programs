#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(){

	/*printf tiene escritura bufferizada*/
	fprintf(stderr, "Hola\n");
	fputs("hola", stderr);
	fprintf(stderr,"\a"); /*stderr no está bufferizado*/
	sleep(1);
	fputc('\a', stderr);
	sleep(1);
	printf("\a\n");


	return EXIT_SUCCESS; 
}	


