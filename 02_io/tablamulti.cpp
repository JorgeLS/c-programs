#include <stdio.h>
#include <stdlib.h>

#define N 11
#define MAX 0x100

void titulo(int num){

        //Crea una función llamada titulo y en esa funcion crea una variable llamada num que luego se usará en esta función

        char titulo[MAX] = "toilet -fpagga --metal Tabla de Multiplicar";

        //Va de 0 a 100 y la 0 es una t y en ASCII

        sprintf(titulo,"toilet -fpagga --gay Tabla del %i", num);
	system(titulo);
	puts("");
}

int main(){

	int a=0;
	int b=0;
	int d=0;
	int e=0;
	int g=0;
	int tabla;

	//This is the simple version
	printf("Dime que tabla de multiplicar quieres que te muestre:");
	scanf("%i", &b);
	puts("\n");

	titulo(b);

	a=0*b;
	printf("%10i\n", a);
	a=0;

	a=1*b;
	printf("%10i\n", a);

	a=2*b;
	printf("%10i\n", a);
	a=0;

	a=3*b;
	printf("%10i\n", a);

	a=4*b;
	printf("%10i\n", a);
	a=0;

	a=5*b;
	printf("%10i\n", a);

	a=6*b;
	printf("%10i\n", a);
	a=0;

        a=7*b;
        printf("%10i\n", a);
	a=0;

	a=8*b;
	printf("%10i\n", a);

	a=9*b;
	printf("%10i\n", a);
	a=0;

	a=10*b;
	printf("%10i\n", a);
	a=0;

	//This versions in with for
	//This is simple of the for
	for (int f=0; f<N; f++){
	printf("%7i\n",f*b);
	}

	//This is other simple of the for
	for (int c=0; c<N; c++){
	d= c*b;
	printf("%5i\v", d);
	}

	//This has a for but with plus insted of multiply
	printf("\n%5i", e);
	for (int c=0; c<10; c++){
	e= e+b;
	printf("\n%5i", e);
	}
	printf("\n");

	//This is more sofisticated than the first
	printf("La tabla del ");
	scanf("%i", &tabla);
	printf("0x%i=%i\n", tabla, 0*tabla);
	printf("1x%i=%i\n", tabla, 1*tabla);
	printf("2x%i=%i\n", tabla, 2*tabla);
	printf("3x%i=%i\n", tabla, 3*tabla);
	printf("4x%i=%i\n", tabla, 4*tabla);
	printf("5x%i=%i\n", tabla, 5*tabla);
	printf("6x%i=%i\n", tabla, 6*tabla);
	printf("7x%i=%i\n", tabla, 7*tabla);
	printf("8x%i=%i\n", tabla, 8*tabla);
	printf("9x%i=%i\n", tabla, 9*tabla);
	printf("10x%i=%i\n", tabla, 10*tabla);


	return EXIT_SUCCESS;
}


