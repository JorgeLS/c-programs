#include <stdio.h>
#include <stdlib.h>

int main(){

    /* Declaracion de variables */

    int a, b, c, d, e, f;

    /* Entrada de Datos */

    printf("Da valor a `a`:");
    scanf("%i", &a);

    printf("Da valor a `b`:");
    scanf("%i", &b);

    printf("Da valor a `c`:");
    scanf("%i", &c);

    printf("Da valor a `d`:");
    scanf("%i", &d);

    printf("Da valor a `e`:");
    scanf("%i", &e);

    printf("Da valor a `f`:");
    scanf("%i", &f);

    /* Declaro lo que va ha hacer el programa */

    printf("Los valores de a, b, c, d, e y f son:\n"
            "%i %i %i %i %i %i\n", a, b, c, d, e, f);

    printf("Vamos a sumar a+b=c\n"
            "Vamos a multiplicar c*d=e\n"
            "Vamos a dividir e/f=a\n"
            "Vamos a hacer la potencia de d^e=b\n");

    c=a+b;
    e=c*d;
    a=f/e;
    b=d^e;
    f++;

    /* Salida de Datos */

    printf("a=%i\n"
            "b=%i\n"
            "c=%i\n"
            "d=%i\n"
            "e=%i\n"
            "f=%i\n", a, b, c, d, e, f);

    return EXIT_SUCCESS;
}
