#include <stdio.h>
#include <stdlib.h>

#define N 100

int main(){

    /* Inicializacion de Datos */

    double lista[N];
    double nota;
    int n_alumnos;

    /* Lectura de Datos */

    printf("Dime cuantos alumnos vas: ");
    scanf(" %i", &n_alumnos);

    for(int i=0; i<n_alumnos; i++){
        printf("%i.- Dime la nota sobre 10: ", i+1);
        scanf(" %lf", &lista[i]);

        //Condicion N < 0 Not Posible
        if(lista[i] < 0)
            return 0;
    }

    /* Cálculo de Datos */

    for(int i=0; i<n_alumnos; i++)
        nota += lista[i];

    /* Salida de Datos */

    printf("La media de la clase es %.2lf\n",nota/n_alumnos);

    return EXIT_SUCCESS;
}
