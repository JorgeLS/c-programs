#include <stdio.h>
#include <stdlib.h>

#define N 10

int main()
{
    int celda[N];

    /* Cálculo de Datos */
    for(int i=0; i<N; i++)
        celda[i] =(i + 1);

    /* Salida de Datos */
    for(int i=0; i<N; i++)
        printf("Celda %i = %i\n", i, celda[i]);

    for(int i=0; i<N; i++){
        for(int j=0; j<celda[i]; j++)
            printf("*");
    printf("\n");
    }

    return EXIT_SUCCESS;
}
