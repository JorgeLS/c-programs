#include <stdlib.h>

int main (int argc, char* argv[]) {

    int suma = 0;

    for (int i = 1; i < argc; i++)
        suma += atoi(argv[i]);

    if (argc < 3)
        return -1;
    else
        return suma;
}
