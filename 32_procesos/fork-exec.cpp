#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int spawn (char* program, char* parameters[]) {

    pid_t child_id;

    child_id = fork ();

    // Padre
    if (child_id != 0) {
        return child_id;
    }
    // Hijo
    else {
        execvp (program, parameters);

        fprintf (stderr, "No se ha podido ejecutar el programa. %s\n", program);

        abort ();
    }

    return child_id;
}

int main (int argc, char* argv[]) {

    int child_status;
    char * arg_list[argc];

    for (int i = 0; i < argc; i++) {
        if (i != argc-1)
            arg_list[i] = argv[i+1];
        else
            arg_list[i] = NULL;
    }

    spawn (arg_list[0], arg_list);

    wait (&child_status);

    if (WIFEXITED (child_status))
        printf ("El return del porceso hijo es %d\n", WEXITSTATUS(child_status));
    else
        printf("No se ejecuto bien el hijo\n");

    printf ("Fin del programa.\n");

    return 0;
}
