#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

sig_atomic_t sigusr1_count = 0;

void handler (int sig_number) {
    sigusr1_count++;
    printf ("SIGUSR1 count = %i\n", sigusr1_count);
}

int main () {

    // Define the struct
    struct sigaction sa;

    // Convert all the struct to 0
    memset (&sa, 0, sizeof(sa));

    // Define the signal handler
    sa.sa_handler = &handler;

    // Create a signal action
    while (1)
    {
        sigaction (SIGINT, &sa, NULL);
        if (sigusr1_count == 3)
            break;
    }

    printf ("I show you a count of your actions.\n");
}
