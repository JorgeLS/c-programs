#include <stdio.h>
#include <dlfcn.h>

int main () {

    double a = 1, b = 12;
    void* handle = dlopen("libaritmetica.so", RTLD_LAZY);
    void (*test) () = (void (*)()) dlsym(handle, "catalogo");

    (*test) ();
    fprintf(stderr, "a");

    dlclose (handle);

    return 0;
}
