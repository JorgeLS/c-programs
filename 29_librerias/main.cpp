#include <stdio.h>

#include "aritmetica.h"

int main ()
{
    printf ("Sumo 1 + 10: %i\n", suma(1, 10));
    printf ("Resto 10 - 1: %i\n", resta(10, 1));
    printf ("Multiplico 2 * 9.5: %.2lf\n", multi(2, 9.5));
    printf ("Divido 50 / 4.5: %.2lf\n", divi(50, 4.5));

    return 0;
}
