#ifndef __ARITMETICA_H__
#define __ARITMETICA_H__

#ifdef __cplusplus
extern "C" {
#endif

    int suma  (int op1, int op2);
    int resta (int op1, int op2);

    double multi (double op1, double op2);
    double divi  (double op1, double op2);

#ifdef __cplusplus
}
#endif

#endif
