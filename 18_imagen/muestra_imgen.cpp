#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define DELAY 5000

int main(int argc, char *argv[]){

    FILE *pf;
    int c, k, b, e;
    int n = 0;

    if(argc < 2){
        pf = fopen("panda.bmp", "r");
    }
    else{
        if( !(pf = fopen(argv[1], "r")) ) {
            fprintf(stderr, "No existe el archivo\n");
            return EXIT_FAILURE;
        }
    }

    system("clear");
    // Busca el tamaño del archivo
    fseek (pf, 18, SEEK_SET); // en la pos 18 del fichero esta el tamaño h
    k = getc(pf) * 3;
    rewind(pf);

    // Donde empieza la imagen
    fseek (pf, 10, SEEK_SET); // en la pos 18 del fichero esta el tamaño h
    b = getc(pf);
    rewind(pf);

    // Donde acaba la imagen
    fseek (pf, 0, SEEK_END); // en la pos 18 del fichero esta el tamaño h
    e = ftell(pf);
    rewind(pf);
/*
    // Dibuja los atributos de la imagen
    if ( argc > 2 ) {
        do{
            c = getc(pf);
            if(n == b)
                n = 0;
            printf("%i - %i\n",n, c);
            n++;

        }while(n != b);
    }

    // Dibuja la imagen en numeros
    fseek (pf, b, SEEK_SET);
    n = 0;
    printf("\n");
    do{
        n++;
        if(n % 3 == 0){
            getchar();
        }
        c = getc(pf);
        // usleep(DELAY)
        printf("%i ", c);

    }while(c != EOF);

    // Dibuja la imagen original
    n = 0;
    fseek (pf, b, SEEK_SET); // En la pos 18 del fichero esta el tamaño H
    printf("\n");
    do{
        c = getc(pf);
        // usleep(DELAY)

            if(n % k == 0)
                printf("\n");
            if(c <= 128)
                printf(" ");
            else if( c <= 226 )
                printf("●");
            else if(c <= 255)
                printf("■");
            else
                printf("%i ", c);
        n++;

    }while(c != EOF);

    printf("\n\n");
*/
    // Dibuja la imagen bien
    for(int i = e/k; i>=0; i--){
        // fseek (pf, b - i, SEEK_SET);
        fseek (pf, k * i + b, SEEK_SET);
        printf("\n");
        // usleep(DELAY);
        for(int j=0; j<k; j++){
            c = getc(pf);
            if( c <= 128 )
                printf(" ");
            else if( c <= 226 )
                printf("●");
            else if(c <= 255 )
                printf("■");
            else
                printf("%i ", c);
        }
    }
/*
    for(int i = (n-k)/k; i>=0; i--){
        fseek (pf, k * i + b, SEEK_SET);
        printf("\n");
        // usleep(DELAY);
        for(int j=0; j<k; j++){
            c = getc(pf);
            if( c == 0)
                printf("+");
            else if(c == 255)
                printf("-");
            else if(c == 226)
                printf("/");
            else if( c == 73 || c == 57 || c == 64 )
                printf("*");
            else
                printf("_");
        }
    }

    for(int i = (n-k)/k; i>=0; i--){
        fseek (pf, k * i + b, SEEK_SET);
        printf("\n");
        // usleep(DELAY);
        for(int j=0; j<k; j++){
            c = getc(pf);
            if( c < 128)
                printf("□");
            else if(c >= 128)
                printf("■");
        }
    }
    */
    printf("\n\n");

    fclose(pf);

    return EXIT_SUCCESS;
}


