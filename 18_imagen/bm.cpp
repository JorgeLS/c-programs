#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define Bpp 3 // Tamaño por pixel

const char *five[] = {"█", "▓", "▒", "░", " "};

int offset, width, height, row_size;

const char * five_colors ( unsigned char r, unsigned char g, unsigned char b ) {

    int media = ( r + g + b ) / 3;

    return five[media / 51];
}

const char * two_colors ( unsigned char r, unsigned char g, unsigned char b, int M ) {

    int media = ( r + g + b ) / 3;

    return media < M ? "░" : "█";
}

void imprimir ( unsigned char *image, int valim, int numCol ) {

    if( numCol == 5 ){
        for (int row=height; row>=0; row-- ){
            for (int col=0; col<width; col++){
                unsigned char * px = &image[row_size * row + Bpp * col]; // Pasa la direccion de memoria de image
                printf("%s", five_colors (
                            px[0],
                            px[1],
                            px[2]) );
            }
            printf ("\n");
            // printf ("%s", five_colors(
            // image[row_size * row + Bpp * col + 0],
            // image[row_size * row + Bpp * col + 1],
            // image[row_size * row + Bpp * col + 2] ) );
        }
    }
    else {
        for (int row=height; row>=0; row-- ){
            for (int col=0; col<width; col++){
                unsigned char * px = &image[row_size * row + Bpp * col]; // Pasa la direccion de memoria de image
                printf("%s", two_colors (
                            px[0],
                            px[1],
                            px[2],
                            valim));
            }
            printf ("\n");
        }
    }
}

void pon_variables (FILE *file) {

    fseek (file, 0xe, SEEK_SET); // 14 - 0x
    offset = getc(file);

    fseek (file, 0x12, SEEK_SET);
    width = getc(file);

    fseek (file, 0x16, SEEK_SET);
    height = getc(file);

    row_size = (width * Bpp + 3) / 4 * 4;
}

int  main(int argc, char *argv[]){

    FILE *pf;

    char BMP_NAME[] = "actress.bmp";

    if (argc < 3) {
        fprintf (stderr, "Pon ./bm [NombreArchivo] [Colores (2-5)]\n");
        return EXIT_FAILURE;
    }
    else
        strcpy(BMP_NAME,argv[1]);

    if ( !(pf = fopen (BMP_NAME, "rb")) ) {
        fprintf (stderr, "Mac donde estas?\nRai no te veo.\n");
        return EXIT_FAILURE;
    }

    pon_variables(pf);

    unsigned char *image;
    image = (unsigned char *) malloc( height * row_size ) ;

    fseek (pf, offset, SEEK_SET);
    fread (image, 1, height * row_size, pf);
    fclose (pf);

    // for(int i=0; i<255; i+=25) {
    //     imprimir ( image, i, atoi (argv[1]) );
    //     usleep (200000);
    //     system("clear");
    //     system("clear");
    // }

    imprimir ( image, 128, atoi (argv[2]) );

    free (image);

    return EXIT_SUCCESS;
}

