#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define DELAY 5000
#define MAX 256

int main(int argc, char *argv[]){

    FILE *pf;
    int c, k = 0;
    int n = 0;
    int size[2];
    int white = 0, // 255
        black = 0; // 0
    const char *info[] = {
        "Tipo de fichero 'BM'",
        "Tamaño del archivo",
        "Reservado",
        "Reservado",
        "Inicio de los datos de la imagen",
        "Tamaño de la cabecera del bitmap",
        "Anchura (píxels)",
        "Altura (píxels)",
        "Número de planos",
        "Tamaño de cada punto",
        "Compresión (0=no comprimido)",
        "Tamaño de la imagen",
        "Resolución horizontal",
        "Resolución vertical",
        "Tamaño de la tabla de color",
        "Contador de colores importantes"
    };

    //Parece ser que el 128 es el azul
    int color_dif[MAX], num_color[MAX];

    for(int i=0; i<MAX; i++){
        color_dif[i] = 0;
        num_color[i] = 0;
    }

    if(argc < 2){
        pf = fopen("nerd.bmp", "r");
    }
    else{
        if( !(pf = fopen(argv[1], "r")) ) {
            fprintf(stderr, "No existe el archivo\n");
            return EXIT_FAILURE;
        }
    }
    system("clear");

    // Dibuja los atributos de la imagen
    do{

        if( n == 0 || n == 6 || n == 8 ||
           n == 26 || n == 28 ) {
            printf("\n%s - Posicion en la imagen [",
                    info[k++]);
            for(int i=1; i<3; i++)
                printf("%i ", n++);
            printf("\b]\n ");
            for(int i=0; i<2; i++){
                c = getc(pf);
                printf("%3i ", c);
            }
            printf("\n");
        }
        else{
            printf("\n%s - Posicion en la imagen [",
                    info[k++]);
            for(int i=0; i<4; i++)
                printf("%i ", n++);
            printf("\b]\n ");
            for(int i=0; i<4; i++){
                c = getc(pf);
                printf("%3i ", c);
            }
            printf("\n");
        }
    }while(n < 54);

    k = 0, n = 0;
    printf("\n");

    // Busca el tamaño del archivo
    fseek (pf, 18, SEEK_SET); // En la pos 18 del fichero esta el size Horizontal
    size[0] = getc(pf);
    fseek (pf, 22, SEEK_SET); // En la pos 18 del fichero esta el size Horizontal
    size[1] = getc(pf);
    printf("El tamaño del archivo es de %ix%i\n\n", size[0], size[1]);
    rewind(pf);

    fseek (pf, 54, SEEK_SET);
    do{
        c = getc (pf);

        if(c == 0)
            black++;
        else if(c == 255)
             white++;
        if( c != -1)
            color_dif[c] += 1;

    }while(c != EOF);

    printf("Hay %i pixeles negros\n", black);
    printf("Hay %i pixeles blancos\n\n", white);
    for(int i=0; i<MAX; i++){
        if(color_dif[i] == 0) continue;
        printf("%3i.- Hay %3i pixeles\n", i, color_dif[i]);
    }

    fclose(pf);

    return EXIT_SUCCESS;
}
