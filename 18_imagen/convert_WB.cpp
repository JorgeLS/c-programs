#include <stdio.h>
#include <stdlib.h>

#define FILENAME "girlWB.bmp"

int main(int argc, char *argv[]){

    FILE *pf;
    int c, size;

    if(argc < 2){
        pf = fopen(FILENAME, "r+");
    }
    else{
        if( !(pf = fopen(argv[1], "r+")) ) {
            fprintf(stderr, "No existe el archivo\n");
            return EXIT_FAILURE;
        }
    }

    fseek (pf, 0, SEEK_END);
    size = ftell(pf);
    fseek (pf, 0, SEEK_SET);

    fseek(pf, 54, SEEK_SET);
    putc (255, pf);
    fseek(pf, 55, SEEK_SET);
    putc (255, pf);
    fseek(pf, 56, SEEK_SET);
    putc (255, pf);

    fseek (pf, 0, SEEK_SET);

    for(int i=58; i<size; i++){
        c = getc(pf);
        if(c <= 128){
            fseek(pf, i, SEEK_SET);
            putc (0, pf);
        }
        else{
            fseek(pf, i, SEEK_SET);
            putc (255, pf);
        }
    }

    fclose(pf);
    return EXIT_SUCCESS;
}


