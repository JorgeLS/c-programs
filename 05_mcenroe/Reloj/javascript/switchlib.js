function fhora(hora){
    var h = 90 - 30 * hora

    linea( p2c(0, h), p2c(200 - 100, h) )
}

function fmins(min){
    var m = 90 - 6 *  min

    linea( p2c(0, m), p2c(200 - 50, m) )
}

function fsecs(sec){
    var s = 90 - 6 * sec

    linea( p2c(0, s), p2c( 200 - 25, s) )
}
