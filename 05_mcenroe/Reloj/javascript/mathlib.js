
function p2c(radio, angulo) {
	return [ radio * Math.cos(rad(angulo)), radio * Math.sin(rad(angulo)) ]
}

function rad(angulo) {
    return angulo * Math.PI / 180
}

function cc(coord){
    return [XC + coord[X], YC - coord[Y]]
}

function linea(p1, p2){
    var c1 = cc(p1)
    var c2 = cc(p2)
    lienzo.moveTo(c1[X], c1[Y])
    lienzo.lineTo(c2[X], c2[Y])
}
