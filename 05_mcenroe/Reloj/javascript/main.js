var X = 0
var Y = 1
var width = 800
var height = 800
var XC = width / 2
var YC = height / 2
var lienzo
var ndiv = 12
var marca = []

var Time = setInterval(
    function () {
        reload ()
        main ()
    }, 1000)
var cortes = 360 / 60

function reload(){
    lienzo.clearRect(0, 0, width, height)
}

function colorl(valor) {
    lienzo.strokeStyle = valor;
}

function main(){

    var hora = new Date().getHours()
    var min  = new Date().getMinutes()
    var sec  = new Date().getSeconds()

    lienzo = document.getElementById("lienzo").getContext("2d")

    var radio = 200
    var extra = 5
    var c = 15

    /* Put the time */
    lienzo.beginPath()
    lienzo.font = "30px Arial"
    lienzo.fillText("Time", 45, 50)
    if(sec < 10)
        lienzo.fillText(hora + ":" + min + ":0" + sec, 50, 80)
    else
        lienzo.fillText(hora + ":" + min + ":" + sec, 50, 80)
    lienzo.stroke()

    /* Calculo dónde están las marcas */
    for (var i=0; i<60; i++){
        marca[i]   =  p2c(radio, c * cortes)
        c = c + 1
    }

    /* CÍRCULO Grande */
    lienzo.beginPath()
    lienzo.arc(XC, YC, radio, 0, Math.PI * 2)
    colorl("#3333CC")
    lienzo.stroke()

    /* MARCAS */
    lienzo.beginPath()
    colorl("#CC3333")
    for (var angulo = 0; angulo < 360; angulo += cortes){
        if(angulo % 5 == 0){
            linea( p2c(radio-10, angulo), p2c(radio+10, angulo) )
        }
        else{
            linea( p2c(radio-extra, angulo), p2c(radio+extra, angulo) )
        }
    }
    lienzo.stroke()

    /* Manecillas */
    //Hours
    lienzo.beginPath()
    colorl("#000000")
    fhora(hora)
    lienzo.stroke()
    //Minutes
    lienzo.beginPath()
    colorl("#33CC33")
    fmins(min)
    lienzo.stroke()
    //Seconds
    lienzo.beginPath()
    colorl("#CC3333")
    fsecs(sec)
    lienzo.stroke()

}

