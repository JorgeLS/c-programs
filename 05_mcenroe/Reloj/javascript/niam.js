/* LA ANTIGUA main.js
   THE PREVIOUS main.js */
var X = 0
var Y = 1
var XC = WIDTH / 2
var YC = HEIGHT / 2
var lienzo
var ndiv = 12
var marca = [ ]
var marcahora = [ ]
var marcamin = [ ]
var marcasec = [ ]

var hora = new Date().getHours()
var min  = new Date().getMinutes()
var sec  = new Date().getSeconds()

/* Muestra los Círculos de la hora y los minutos Mostrar poner 1 */
var circulos = 0
var cortes = 360 / ndiv

function colorl(valor) {
    lienzo.strokeStyle = valor;
}

function cc(coord){
    return [XC + coord[X], YC - coord[Y]]
}

function linea(p1, p2){
    var c1 = cc(p1)
    var c2 = cc(p2)
    lienzo.moveTo(c1[X], c1[Y])
    lienzo.lineTo(c2[X], c2[Y])
}

function main() {

    lienzo = document.getElementById("lienzo").getContext("2d")

    var radio     = 200
    var radiohora = 100
    var radiomin  = 150
    var radiosec  = 175
    var extra = 5
    var c = 3

    /* Te pone el tiempo */
    lienzo.beginPath()
    lienzo.font = "30px Arial"
    lienzo.fillText("Time", 45, 50)
    lienzo.fillText(hora + " : " + min + " : " + sec, 50, 80)
    lienzo.stroke

    /* Calculo dónde están las marcas */
    for (var i=0; i<ndiv; i++){
        marca[i]   =  p2c(radio, c * cortes)
        c = c + 1
    }
    for (var i=0; i<ndiv; i++){
        marcahora[i] =  p2c(radiohora, c * cortes)
        c = c + 1
    }
    for (var i=0; i<60; i++){
        marcamin[i]  =  p2c(radiomin,  c * cortes / 5)
        c = c + 1
    }
    for (var i=0; i<60; i++){
        marcasec[i]  =  p2c(radiosec,  c * cortes / 5)
        c = c + 1
    }

    /* CÍRCULO Grande */
    lienzo.beginPath()
    lienzo.arc(cc([0,0])[X], cc([0,0])[Y], radio, 0, Math.PI * 2)
    colorl("#3333CC")
    lienzo.stroke()

    /* MARCAS */
    lienzo.beginPath()
    colorl("#CC3333")
    for (var angulo = 0; angulo < 360; angulo += cortes)
        linea( p2c(radio-extra, angulo), p2c(radio+extra, angulo) )
    lienzo.stroke()

    /* MANECILLAS */
    lienzo.beginPath()
    if(hora > 12)
        hora = hora - 12
    fhora(hora)
    lienzo.stroke()

    lienzo.beginPath()
    fmins(min)
    lienzo.stroke()

    lienzo.beginPath()
    fsecs(sec)
    lienzo.stroke()

    /* Flechas */
    lienzo.beginPath()
    //lienzo.rotate(20 * Math.PI / 180)
    lienzo.fillRect(XC - marcamin[min][X], YC - marcamin[min][Y], 10, 10)
    lienzo.stroke()

    lienzo.beginPath()
    //lienzo.rotate(20 * Math.PI / 180)
    lienzo.fillRect(XC - marcahora[hora][X], YC - marcahora[hora][Y], 10, 10)
    lienzo.stroke()

    lienzo.beginPath()
    //lienzo.rotate(20 * Math.PI / 180)
    lienzo.fillRect(XC - marcasec[sec][X], YC - marcasec[sec][Y], 10, 10)
    lienzo.stroke()

    /* NUMS HORAS */
    lienzo.beginPath()
    fnumshora()
    lienzo.stroke()

    /* NUMS MINUTOS */
    lienzo.beginPath()
    fnumsmin()
    lienzo.stroke()

    if(circulos == 1){
        /* Círculo Horas */
        lienzo.beginPath()
        lienzo.arc( cc([0,0])[X], cc([0,0])[Y], radiohora, 0, Math.PI * 2 )
        colorl("#3333CC")
        lienzo.stroke()

        /* Círculo Minutos */
        lienzo.beginPath()
        lienzo.arc( cc([0,0])[X], cc([0,0])[Y], radiomin, 0, Math.PI * 2 )
        colorl("#3333CC")
        lienzo.stroke()

        /* Círculo Segundos */
        lienzo.beginPath()
        lienzo.arc( cc([0,0])[X], cc([0,0])[Y], radiosec, 0, Math.PI * 2 )
        colorl("#3333CC")
        lienzo.stroke()

        /* MARCAS HORAS */
        lienzo.beginPath()
        colorl("#CC3333")
        for (var angulo = 0; angulo < 360; angulo += cortes)
            linea( p2c(radiohora-extra, angulo), p2c(radiohora+extra, angulo) )
        lienzo.stroke()

        /* MARCAS MINUTOS */
        lienzo.beginPath()
        colorl("#CC3333")
        for (var angulo = 0; angulo < 360; angulo += cortes)
            linea( p2c(radiomin-extra, angulo), p2c(radiomin+extra, angulo) )
        lienzo.stroke()

        /* MARCAS SEGUNDOS */
        lienzo.beginPath()
        colorl("#CC3333")
        for (var angulo = 0; angulo < 360; angulo += cortes)
            linea( p2c(radiosec-extra, angulo), p2c(radiosec+extra, angulo) )
        lienzo.stroke()
    }
}

