
function fnumshora(){
    colorl("#FF00FF")
    lienzo.font = "15px Arial";

    lienzo.fillText("1",  XC - 4  - marca[0][X] , YC - 14 - marca[0][Y])
    lienzo.fillText("2",  XC + 4  - marca[1][X] , YC - 10 - marca[1][Y])
    lienzo.fillText("3",  XC + 10 - marca[2][X] , YC - 0  - marca[2][Y])
    lienzo.fillText("4",  XC + 14 - marca[3][X] , YC + 4  - marca[3][Y])
    lienzo.fillText("5",  XC + 10 - marca[4][X] , YC + 14 - marca[4][Y])
    lienzo.fillText("6",  XC + 4  - marca[5][X] , YC + 20 - marca[5][Y])
    lienzo.fillText("7",  XC - 4  - marca[6][X] , YC + 22 - marca[6][Y])
    lienzo.fillText("8",  XC - 14 - marca[7][X] , YC + 20 - marca[7][Y])
    lienzo.fillText("9",  XC - 20 - marca[8][X] , YC + 14 - marca[8][Y])
    lienzo.fillText("10", XC - 26 - marca[9][X] , YC + 4  - marca[9][Y])
    lienzo.fillText("11", XC - 24 - marca[10][X], YC - 4  - marca[10][Y])
    lienzo.fillText("12", XC - 14 - marca[11][X], YC - 10 - marca[11][Y])

    /*
    1-  XC+100 ,YC-180  or  XC+ 6-marca , YC-10-marca
    2-  XC+180 ,YC-100  or  XC+10-marca , YC- 2-marca
    3-  XC+210 ,YC+5    or  XC+16-marca , YC+ 4-marca
    4-  XC+180 ,YC+115  or  XC+12-marca , YC+14-marca
    5-  XC+105 ,YC+190  or  XC+ 6-marca , YC+20-marca
    6-  XC-5   ,YC+220  or  XC- 4-marca , YC+22-marca
    7-  XC-110 ,YC+190  or  XC-14-marca , YC+20-marca
    8-  XC-190 ,YC+110  or  XC-16-marca , YC+10-marca
    9-  XC-215 ,YC+5    or  XC-20-marca , YC+ 4-marca
    10- XC-195 ,YC-105  or  XC-24-marca , YC- 2-marca
    11- XC-115 ,YC-180  or  XC-16-marca , YC-10-marca
    12- XC-10  ,YC-210  or  XC- 8-marca , YC-10-marca
    */
}

function fnumsmin(){
    var c = 0
    colorl("#000000")
    lienzo.font = "10px Arial";

    lienzo.fillText("00", XC -  5 - marca[0][X] , YC + 15 - marca[0][Y])
    lienzo.fillText("05", XC - 10 - marca[1][X] , YC + 15 - marca[1][Y])
    lienzo.fillText("10", XC - 18 - marca[2][X] , YC + 14 - marca[2][Y])
    lienzo.fillText("15", XC - 20 - marca[3][X] , YC +  4 - marca[3][Y])
    lienzo.fillText("20", XC - 18 - marca[4][X] , YC -  2 - marca[4][Y])
    lienzo.fillText("25", XC - 12 - marca[5][X] , YC -  7 - marca[5][Y])
    lienzo.fillText("30", XC -  5 - marca[6][X] , YC - 10 - marca[6][Y])
    lienzo.fillText("35", XC +  2 - marca[7][X] , YC -  7 - marca[7][Y])
    lienzo.fillText("40", XC + 10 - marca[8][X] , YC -  2 - marca[8][Y])
    lienzo.fillText("45", XC + 10 - marca[9][X] , YC +  4 - marca[9][Y])
    lienzo.fillText("50", XC + 10 - marca[10][X], YC + 10 - marca[10][Y])
    lienzo.fillText("55", XC -  0 - marca[11][X], YC + 15 - marca[11][Y])
}
