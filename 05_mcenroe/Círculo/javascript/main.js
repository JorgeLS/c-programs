var X = 0  //Esta en la coordenada 0 [0, 1, 2, 3, 4, 5 ...]
var Y = 1  //Esta en la coordenada 1
var XC = WIDTH / 2
var YC = HEIGHT / 2
var lienzo

var marcas = [ ]

function colorl(valor) {
    lienzo.strokeStyle = valor;
}

function cc(coord){
    return [XC + coord[0], YC - coord[1]] //Te dice que [XC + X, YC - Y]
    // Te devuelve un array
}

function linea(p1, p2){
    var c1 = cc(p1)
    var c2 = cc(p2)
    lienzo.moveTo(c1[X], c1[Y])
    lienzo.lineTo(c2[X], c2[Y])
}

function clear(){
    lienzo.clearRect(0, 0, WIDTH, HEIGHT)
}

function main(){

    var radio = 250
    var extra = 10

    lienzo = document.getElementById("lienzo").getContext("2d")
    borrar = document.getElementById("borra").checked
    ndiv   = parseInt(document.getElementById("divi").value)
    tabla  = parseInt(document.getElementById("table").value)

    if(borrar)
        clear();

    for (var i=0; i<ndiv; i++)
    marcas[i] = p2c(radio, i * 360 / ndiv)

    lienzo.beginPath()
    // Te dice que X=0 y Y=0 y que solo coja la X o la Y con el [X] o el [Y]
    lienzo.arc(cc([0,0])[X], cc([0,0])[Y], radio, 0, Math.PI * 2)
    colorl("#3333CC")
    lienzo.stroke()

    lienzo.beginPath()
    colorl("#CC3333")
    for (var angulo = 0; angulo < 360; angulo += 360 / ndiv)
        linea( p2c(radio-extra, angulo), p2c(radio+extra, angulo))
    lienzo.stroke()

    lienzo.beginPath()
    for (var i=1; i<ndiv; i++)
      linea(marcas[i], marcas[tabla*i % ndiv])
    colorl("#000066")
    lienzo.stroke()

    }


