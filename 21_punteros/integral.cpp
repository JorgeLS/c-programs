#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INC 0.0001

// Evalua el valor de un polinomio de coeficientes coef en el punto x
double pol (double *pol, int grado, double x) {

    double resultado = 0;
    int lon = (grado + 1) * sizeof (double);
    double *copia = (double *) malloc ( lon );

    memcpy (copia, pol, lon);

    for (int i=0; i<grado; i++)
        for (int celda=0; celda<=i; celda++)
            *(copia + celda) *= x;

    for (int i=0; i<=grado; i++)
        resultado += *(copia + i);

    free (copia);

    return resultado;
}

double integral (double li, double ls, double (*pf) (double) ) {
    double area = 0;
    for (double x = li; x < ls; x += INC)
        area += INC * (*pf)(x);

    return area;
}

double parabola (double x) {
    double coef[] = {1, 0, 0};
    return pol (coef, 2, x);
}

int main(int argc, char *argv[]){

    printf("f(2) = %.lf\n", parabola(2));
    printf("f(3) = %.lf\n", parabola(3));
    printf("El área es: %.2lf\n", integral (atoi(argv[1]), atoi(argv[2]), &parabola));

    return EXIT_SUCCESS;
}
