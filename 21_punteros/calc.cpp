#include <stdio.h>
#include <stdlib.h>

#define N 2

enum Opcion {
    suma,
    resta,
    multiplicacion,
    division,
    OPCIONES
};

const char *texto[] = {
    "Suma",
    "Resta",
    "Multiplicacion",
    "Division"
};

const char simbolos[] = "+-x/";

double sum(double op1, double op2) {return op1+op2;}
double res(double op1, double op2) {return op1-op2;}
double mul(double op1, double op2) {return op1*op2;}
double div(double op1, double op2) {return op1/op2;}

void titulo () {

    system ("clear");
    system ("toilet -fpagga CALCULADORA");
    printf("\n\n");
}

enum Opcion menu () {

    int opcion;

    do {
        titulo();
        printf("\tOpciones:");
        printf("\n\t==========\n\n");
        for (int i=0; i<OPCIONES; i++) {
            printf("\t %i.- %s\n", i+1, texto[i]);
        }

        printf("\n Opcion: ");
        scanf(" %i", &opcion);
        opcion--;
    } while( opcion < suma || opcion >= OPCIONES );

    return (enum Opcion) opcion;
}

void pide_op (double ops[N]) {

    for(int i=0; i<N; i++) {
        printf("Operando %i: ", i+1);
        scanf(" %lf", &ops[i]);
    }
}

int main(int argc, char *argv[]){

    enum Opcion op;
    double operando[N];
    double resultado;
    double (*operaciones []) (double, double) = {
        &sum, &res, &mul, &div, NULL
    };

    op = menu ();

    pide_op (operando);

    titulo();
    printf("%s %.2lf %c %.2lf = %lf\n\n",
            texto[op],
            operando[0],
            simbolos[op],
            operando[1],
            (*operaciones[op]) (operando[0], operando[1]) );

    return EXIT_SUCCESS;
}


