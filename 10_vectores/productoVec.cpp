#include <stdio.h>
#include <stdlib.h>

#define DIM 3
#define nVec 2
#define X 0
#define Y 1
#define Z 2

int main(){

    double vec[nVec][DIM],
           vecT[DIM];

    for(int i=0; i<nVec; i++){
        printf("Dime tu Vector %i: ", i + 1);
        scanf(" %lf, %lf, %lf", &vec[i][X], &vec[i][Y], &vec[i][Z]);
    }

    vecT[X] = vec[0][Y] * vec[1][Z] - vec[0][Z] * vec[1][Y];
    vecT[Y] = vec[0][Z] * vec[1][X] - vec[0][X] * vec[1][Z];
    vecT[Z] = vec[0][X] * vec[1][Y] - vec[0][Y] * vec[1][X];

    printf("El vector final es: ");

    for(int i=0; i<DIM; i++)
        printf("%.2lf, ", vecT[i]);

    printf("\b\b\n");

    return EXIT_SUCCESS;
}

