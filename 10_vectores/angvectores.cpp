#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define X 0
#define Y 1
#define DIM 2
#define nVEC 2
#define CONG 180 / M_PI

double modulo(double vec[nVEC]){
    return sqrt(
            pow(vec[X], 2) +
            pow(vec[Y], 2)
            );
}

int main(){

    double vec[nVEC][DIM],
           productoE,
           angulo;

    for(int i=0; i<nVEC; i++){
        printf("Vector %i: ", i + 1);
        scanf(" %lf, %lf", &vec[i][X], &vec[i][Y]);
    }

    for(int i=0; i<nVEC; i++)
        productoE += vec[0][i] * vec[1][i];

    angulo = acos( productoE / modulo(vec[0]) / modulo(vec[1]));

    //Salida

    printf("El ángulo de tus vectores es %.2lfrad o %.2lfº\n", angulo, angulo * CONG);

    return EXIT_SUCCESS;
}


