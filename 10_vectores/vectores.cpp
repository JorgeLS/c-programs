#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PI  3.14159265

int main(){

    int DIM, nvec;
    int vec[nvec][DIM];
    double modulo[nvec],
           alfa;

    printf("Cuantas dimensiones tiene tu vector\?: ");
    scanf(" %i", &DIM);
    printf("Cuantos vectores quieres?: ");
    scanf(" %i", &nvec);

    /* Componentes */

    for(int a=0; a<nvec; a++){
        for(int b=0; b<DIM; b++){
            printf("Dime el componente %i del vector%i: ", b, a);
            scanf(" %i", &vec[a][b]);
            printf("%i\n", vec[a][b]);
        }
        printf("%i \t %i   %i\n\n",a,vec[a][0], vec[a][0]);
    }
    printf(" %i %i %i %i\n", vec[0][0], vec[0][1], vec[1][0], vec[1][1]);

    /* Calculador de Modulos */

    for(int a=0; a<nvec; a++){
        for(int b=0; b<DIM; b++){
            modulo[a] += pow(vec[a][b], 2);
            printf("%i %i\t %.2lf\n ", a,b,  modulo[a]);
        }
        modulo[a] = sqrt(modulo[a]);
        printf("%.2lf\n", modulo[a]);
    }

    printf("%.2lf %.2lf\n", modulo[0], modulo[1]);
    //Modulo vector
    /*
    alfa = atan(vec[1] / vec[0]) * 180 / PI;
    //Tambien puede ser:
    //alfa = atan2(vec[1], vec[0]) * 180 / PI;

    printf("El modulo de tu vector es %.2lf\n"
           "El angulo entre i y j es %.2lfº\n", modulo, alfa);
*/
    return EXIT_SUCCESS;

}

