#include <stdio.h>   //Stadard input output, this is a library
#include <stdlib.h>   //Another library
/*Principal funtion*/
int main(){
	int i=0;   //Declare a funtion
	printf("Hello World!!\n");   //Show in the terminal and the \n do a page break
	scanf("%i",&i);   //Scan your funtion 
        printf("%i" "\n", i);   //Show the number that you put
	return 0;   //This must be in the end of the program
	return EXIT_SUCCESS; //Is the same than return 0 but with library stdlib.h
}
